module.exports = {
  root: true,
  ignorePatterns: ["projects/**/*", "*.spec.ts", "modules/**/*", "*.gql"],
  overrides: [
    // TypeScript
    {
      files: ["*.ts"],

      parserOptions: {
        project: ["tsconfig.json"],
        createDefaultProgram: true,
        tsconfigRootDir: __dirname,
        sourceType: "module",
      },
      extends: [
        "plugin:@typescript-eslint/eslint-recommended",
        "plugin:@typescript-eslint/recommended-requiring-type-checking",
        "plugin:@angular-eslint/recommended",
        "plugin:@angular-eslint/template/process-inline-templates",
      ],
      rules: {
        "@typescript-eslint/unbound-method": [
          "error",
          {
            "ignoreStatic": true
          }
        ],
        "@angular-eslint/component-selector": [
          "error",
          {
            type: "element",
            prefix: "alk",
            style: "kebab-case",
          },
        ],
        "@angular-eslint/directive-selector": [
          "error",
          {
            type: "attribute",
            prefix: "alk",
            style: "camelCase",
          },
        ],
        "no-unused-vars": "off",
        "@typescript-eslint/no-unused-vars": ["warn", {args: "after-used", argsIgnorePattern: "^_", destructuredArrayIgnorePattern: "^_", caughtErrors: "none"}],
        "no-trailing-spaces": "error",
        "comma-dangle": ["error", "always-multiline"],
        "no-prototype-builtins": "off",
        semi: ["error", "always", {omitLastInOneLineBlock: true}],
        "space-unary-ops": [
          "error",
          {
            words: true,
            nonwords: false,
          },
        ],
        "no-func-assign": "error",
        "no-obj-calls": "error",
        "no-extra-semi": "error",
        "no-extra-parens": "off",
        "no-extra-boolean-cast": "error",
        "no-ex-assign": "error",
        "no-empty": ["error", {allowEmptyCatch: false}],
        "no-duplicate-case": "error",
        "no-lonely-if": "error",
        "no-dupe-else-if": "error",
        "no-dupe-args": "error",
        "no-constant-condition": "error",
        "no-cond-assign": "error",
        "no-await-in-loop": "error",
        "space-infix-ops": ["error", {int32Hint: false}],
        "array-bracket-spacing": [
          "error",
          "always",
          {
            singleValue: false,
            objectsInArrays: false,
            arraysInArrays: false,
          },
        ],
        "object-curly-spacing": [
          "error",
          "always",
          {
            arraysInObjects: false,
            objectsInObjects: false,
          },
        ],
        "keyword-spacing": ["error", {before: true, after: true}],
        "semi-spacing": ["error", {before: false, after: true}],
        "comma-spacing": ["error", {before: false, after: true}],
        "block-spacing": "error",
        "computed-property-spacing": ["error", "never", {enforceForClassMembers: true}],
        "space-in-parens": ["error", "always", {exceptions: ["{}", "[]", "()", "empty"]}],
        "@typescript-eslint/quotes": ["error", "backtick"],
        // "no-multi-spaces": ["error", { ignoreEOLComments: false, exceptions: { "Property": true } }],
        "no-multi-spaces": "off",
        "key-spacing": ["error", {beforeColon: false, afterColon: true, mode: "minimum", align: "value"}],
        "spaced-comment": ["error", "always", {exceptions: ["*", "-", "+"]}],
        "@typescript-eslint/restrict-template-expressions": [
          "warn",
          {
            allowBoolean: true,
            allowNumber: true,
            allowNullish: true,
            allowAny: false,
          },
        ],
        "max-len": [
          "error",
          {
            code: 160,
            tabWidth: 2,
            comments: 180,
            ignoreTrailingComments: true,
            ignoreStrings: true,
            ignoreRegExpLiterals: true,
            ignorePattern: "^\\s*((let|const )?(\\w+\\s*)[=:]\\s*(\\(.*?\\)\\s=>\\s)?\\$localize|\\$localize)|^s*@Args|return this.initError|throw new",
          },
        ],
        "arrow-body-style": ["error", "as-needed"],
        curly: ["error", "all"],
        "arrow-parens": ["error", "always"],
      },
    },
    // HTML
    {
      files: ["*.html"],
      extends: ["plugin:@angular-eslint/template/recommended"],
      rules: {
        "max-len": ["error", {code: 150}],
        "no-multi-spaces": ["error", {ignoreEOLComments: false, exceptions: {Property: true}}],
      },
    },
    // JS
    {
      files: ["*.js"],
      extends: ["eslint:recommended"],
      env: {
        browser: true,
        node: true,
      },
    },
  ],
};
