import { Injectable } from '@angular/core';
import { catchError, map, merge, Observable, of, switchMap, take, takeUntil } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { EnvService } from '../../core/services/env/env.service';

import {
  ResponseFileApi,
  responseFileApiValidator,
} from '../../core/models/api/response-file-api';
import {
  ResponsePatchData,
} from '../../core/models/api/response-patch-data';
import { Contribution } from '../../core/models/contribution';
import { DataFile } from '../../core/models/data-file.model';


@Injectable({
  providedIn: `root`,
})
export class HomeService {
  private header = { 'Accept': `application/ld+json`, 'Content-Type': `application/ld+json` };
  private headerFile = { 'Accept': `application/ld+json` };
  private headerInitialize = { 'Accept': `application/ld+json`, 'Content-Type':  `application/merge-patch+json` };
  private contributionId!: number;

  private type!: string;

  constructor(
    private httpClient: HttpClient,
    private envService: EnvService,
  ) { /* empty */
  }

  /**
   * Envoie de fichier un par un
   * @return un observable qui se déclenche pour chaque fichier envoyé au serveur
   */
  postFileList( fileToUploads: FileList, contribution?: Contribution ): Observable<ResponseFileApi> {
    console.log( ` THIS `, this.contributionId );
    const getData = contribution || this.contributionId ? of( <ResponsePatchData>contribution ) : this.createContribution();

    return getData.pipe(
      switchMap(( contribution ) => {
        if ( !this.contributionId ){
          this.contributionId = contribution.id;
          this.type = contribution[`@type`];
        }

        // conversion de Filelist en tableau
        let idx = 0;
        const obsList: Observable<ResponseFileApi>[] = [];
        while ( idx < fileToUploads.length ) {
          obsList.push( this.postFile( fileToUploads[idx]));
          idx = idx + 1;
        }

        return merge( ...obsList );
      }),
      );
  }


  /**
   * Renvoie la liste des nom de fichiers à partir d'un FileList
   */
  getFilenameFromList( fileList: FileList ): string[] {
    if ( !fileList ) {
      return [];
    }

    let idx = 0;
    const filename: string[] = [];
    while ( idx < fileList.length ) {
      filename.push( fileList[idx].name );
      idx = idx + 1;
    }

    return filename;
  }

  /**
   * Pour finir l'envoi des fichier
   */
  finaliseFileTransfert( contribution?: Contribution ): Observable<number> {
    const urlType = contribution ? `recreate_data_sets` : `initialize`;

    return this.httpClient.patch<ResponsePatchData>(
      `${this.envService.urlContribution}/api/data/${urlType}/${this.contributionId}`, { status: `api/statuses/3`, '@type': this.type },
      { withCredentials: true, headers: this.headerInitialize },
    ).pipe(
      map(( contribution ) => contribution.id ),
    );
  }


  /**
   * envoie d'un fichier au serveur, ajout le nom du fichier dans la réponse
   */
  private postFile( fileToUpload: File ): Observable<ResponseFileApi> {
    const url = `${this.envService.urlContribution}/api/data_files`;
    const formData: FormData = new FormData();

    formData.append( `file`, fileToUpload, fileToUpload.name );
    formData.append( `data`, `api/data/${this.contributionId}` );

    return this.httpClient.post<ResponseFileApi>( url, formData, { withCredentials: true, headers: this.headerFile }).pipe(
      map(( responseFileApi ) => {
        responseFileApi.name = fileToUpload.name;
        return responseFileApiValidator.parse( responseFileApi );
      }),
      catchError(( error ) => {
        console.error( error );
        return of({ name: fileToUpload.name, error: error as string } as ResponseFileApi );
      }),
    );
  }

  removeFile( id: number ){
    return this.httpClient.delete( `${this.envService.urlContribution}/api/data_files/${id}` );
  }

  loadDataFile( contribution: Contribution ): Observable<DataFile>{
    if ( contribution?.datafiles?.length ){
      const obs: Observable<DataFile>[]  = [];
      contribution.datafiles.forEach(( datafile ) =>{
        obs.push( this.httpClient.get<DataFile>( `${this.envService.urlContribution}${datafile}` ));
      });

      return merge( ...obs );
    }

    return of( null );
  }

  private createContribution(): Observable<ResponsePatchData>{
    return this.httpClient.post<ResponsePatchData>( `${this.envService.urlContribution}/api/data`, [],
      { withCredentials: true, headers: this.header });
  }
}
