import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from "./home-routing.module";
import { CommonModule } from "@angular/common";
import { DragDropFileModule } from '../../core/directives/drag-drop-file/drag-drop-file.module';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    DragDropFileModule,
  ],
  exports:      [],
  declarations: [
    HomeComponent,
  ],
})
export class HomeModule {
}
