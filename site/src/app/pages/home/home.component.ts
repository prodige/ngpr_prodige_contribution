import { Component, OnInit } from '@angular/core';
import { HomeService } from "./home.service";
import { ActivatedRoute, Router } from '@angular/router';
import { IFileLoading } from '../../core/models/file-loading-type';
import { BaseComponent } from '../../core/components/base/base.component';
import { from, of, switchMap, takeUntil } from 'rxjs';
import { Contribution, CONTRIBUTION_STATUS } from '../../core/models/contribution';
import { LoggerService } from '../../core/components/logger/logger.service';
import { LOGGER_MESSAGE } from '../../core/models/logger-message';

@Component({
  selector:    `alk-home`,
  templateUrl: `./home.component.html`,
  styleUrls:   [`./home.component.scss`],
})
export class HomeComponent extends BaseComponent implements OnInit{
  public fileToUploads!: FileList|null;
  public fileLoading: IFileLoading = {};
  public fileNameId: {[key: string]: number} = {};

  public fileTransfertChecked = true;

  public transfertEnding = false;

  public finalizeLoading = false;

  public contribution: Contribution = null;

  public canStart = false;

  public editeMod = false;

  public deleteLoading = false;

  constructor(
    private homeService: HomeService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private loggerService: LoggerService,
  ) {
    super();

  }

  ngOnInit(): void {
    this.fileLoading = {};
    this.activatedRoute.data.pipe(
      takeUntil( this.endSubscriptions ),
      switchMap(({ contribution }) => {
        if ( !contribution ){
          return of( null );
        }
        this.contribution = <Contribution>contribution;
        this.editeMod = this.contribution.status === CONTRIBUTION_STATUS.edited;
        return this.homeService.loadDataFile( this.contribution );
      }),
    )
      .subscribe(( result ) =>{
        if ( result?.contentUrl ){
          this.fileLoading[result.contentUrl] = `success`;
          this.fileNameId[result.contentUrl] = result.id;
        }
      });
  }

  handleFileInput( event: Event ): boolean {
    if ( !( event.target as HTMLInputElement ).files ) {
      return false;
    }
    this.fileToUploads = ( event.target as HTMLInputElement ).files;

    this.prepareFiles();
    this.uploadFiles();

    return true;
  }


  uploadFiles() {
    Object.keys( this.fileLoading ).forEach(( key ) => {
      if ( this.fileLoading[key] === `no` ){
        this.fileLoading[key] = `loading`;
      }
    });

    this.transfertEnding = false;
    this.fileTransfertChecked = true;

    this.homeService
      .postFileList( this.fileToUploads, this.contribution  )
      .pipe( takeUntil( this.endSubscriptions ))
      .subscribe({
        next: ( apiResponse ) => {
          if ( this.fileLoading[apiResponse.name] && !apiResponse[`error`]) {
            this.fileNameId[apiResponse.name] = apiResponse.id;
            this.fileLoading[apiResponse.name] = `success`;
          } else {
            this.fileLoading[apiResponse.name] = `error`;
            this.fileTransfertChecked = false;
          }
        },
        error:    ( err ) => { console.error( err ) },
        complete: () =>{
          this.transfertEnding = true;

        },
      });

  }

  editResource(){
    if ( this.contribution && !this.fileToUploads ){
      this.router.navigate([ `contribute/form`, this.contribution.id ]).catch( console.error );
      return;
    }
  }

  finalizeTransfert(): void{
    this.finalizeLoading = true;
    this.transfertEnding = false;

    this.homeService.finaliseFileTransfert( this.contribution ).pipe(
      takeUntil( this.endSubscriptions ),
      switchMap(( contributionId ) => from( this.router.navigate([ `contribute/form`, contributionId ]))),
    )
      .subscribe({
        error: ( err ) => {
          console.error( err );
          this.loggerService.errorOnModal$( LOGGER_MESSAGE.initializeFileError );
          this.finalizeLoading = false;
        },
        next: () =>{
          this.finalizeLoading = false;
        },
      });
  }

  public removeFile( filename: string ){
    if ( !this.fileNameId[filename]){
      return;
    }

    this.deleteLoading = true;
    this.homeService.removeFile( this.fileNameId[filename]).pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe({
        next: () =>{
          this.deleteLoading = false;
          delete this.fileLoading[filename];
        },
        error: ( err ) => {
          this.deleteLoading = false;
          console.error( err );
          this.loggerService.errorOnModal$( LOGGER_MESSAGE.deleteFileError );
        },
      });
  }

  public onDrop( event: FileList ){
    console.log( `onDrop`, event );
    this.fileToUploads = event;
    this.prepareFiles();
    this.uploadFiles();
  }

  private prepareFiles(){
    // mise à jour de l'état des fichiers
    const filenames = this.homeService.getFilenameFromList( this.fileToUploads );

    filenames.forEach(( filename ) => {
      if ( !this.fileLoading[filename]){
        this.fileLoading[filename] = `no`;
      }

    });


  }
}
