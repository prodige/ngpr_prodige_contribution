import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataOverviewComponent } from './data-overview.component';
import {RouterTestingModule} from '@angular/router/testing';
import {CommonModule} from '@angular/common';
import {MapModule, VisualiseurCoreModule} from '@alkante/visualiseur-core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FieldDisplayModule} from '../../../core/components/field-display/field-display.module';
import {FormErrorModule} from '../../../core/components/form-error/form-error.module';

describe('DataOverviewComponent', () => {
  let component: DataOverviewComponent;
  let fixture: ComponentFixture<DataOverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataOverviewComponent ],
      imports: [
        RouterTestingModule,
        CommonModule,
        VisualiseurCoreModule,
        MapModule,
        FormsModule,

        FieldDisplayModule,
        ReactiveFormsModule,
        FormErrorModule,
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
