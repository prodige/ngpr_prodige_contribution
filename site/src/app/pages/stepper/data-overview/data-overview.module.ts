import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataOverviewComponent } from './data-overview.component';
import { MapModule, VisualiseurCoreModule } from '@alkante/visualiseur-core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FieldDisplayModule } from '../../../core/components/field-display/field-display.module';
import { FormErrorModule } from '../../../core/components/form-error/form-error.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    VisualiseurCoreModule,
    MapModule,
    FormsModule,

    FieldDisplayModule,
    ReactiveFormsModule,
    FormErrorModule,
    NgbModule,
  ],
  exports: [
    DataOverviewComponent,
  ],
  declarations: [
    DataOverviewComponent,
  ],
})
export class DataOverviewModule { }
