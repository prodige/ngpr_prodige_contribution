import {
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { StepperService } from '../stepper.service';
import { StepType } from '../../../core/models/step-type';
import { MapIdService, MapService, VisualiseurCoreService } from '@alkante/visualiseur-core';
import { BaseComponent } from '../../../core/components/base/base.component';
import { delay, finalize, of, switchMap, takeUntil } from 'rxjs';
import { ContributionService } from '../../../core/services/contribution.service';
import { Contribution, ContributionExtent, ContributionField } from '../../../core/models/contribution';
import { PRODIGE_TYPE } from '../../../core/models/prodige-type';
import { PRODIGE_ENCODAGE } from '../../../core/models/prodige-encodage';
import { ContextService } from '../../../core/services/context.service';
import { Map } from 'ol';
import { FormArray, FormGroup } from '@angular/forms';
import { LoggerService } from '../../../core/components/logger/logger.service';

@Component({
  selector:    `alk-data-overview`,
  templateUrl: `./data-overview.component.html`,
  styleUrls:   [`./data-overview.component.scss`],
})
export class DataOverviewComponent extends BaseComponent implements OnInit, OnDestroy {
  public prodigeEncodage = PRODIGE_ENCODAGE;

  public prodigeTypeKeys = Object.keys( PRODIGE_TYPE );
  public prodigeType = PRODIGE_TYPE;

  public canDisplayMap = false;

  public totalEntities = 40; // TODO: temporaire en attendant l'accès aux données

  public contribution!: Contribution;

  public fieldDisplayData: ContributionField[];

  public contributionForm: FormGroup;
  public extentForm: FormGroup;
  public fieldForm: FormArray;

  private mapId = `visualiseur`;

  private map: Map;

  constructor(
    private stepperService: StepperService,
    private coreService: VisualiseurCoreService,
    private mapService: MapService,
    private mapIdService: MapIdService,
    private contributionService: ContributionService,
    private contextService: ContextService,
    private loggerService: LoggerService,
  ) {
    super();

  }

  ngOnInit(): void {
    this.mapService.setMapId( `visualiseur`, this.mapId || `main` );
    this.mapIdService.setId( this.mapId || `main` );

    this.mapService.getMapAfterComplete( this.mapId ).pipe(
        takeUntil( this.endSubscriptions ),
      )
      .subscribe(( map )=>{
        this.map = map;
        this.contextService.drawContextEtent( map, this.contribution.extent,  <string> this.contribution.sourceSRS );
      });

    this.contributionService.getContribtuion$().pipe(
      takeUntil( this.endSubscriptions ),
      delay( 250 ), // tempo pour que le DOM alk-ol-map soit à la bonne taille,
      switchMap(( contribution ) =>{
        this.contribution = contribution;

        this.contributionForm = <FormGroup> this.contributionService.getContributionForm().controls[`DataOverview`];

        this.fieldForm = <FormArray> this.contributionForm.get( `fields` );

        this.updateFields();

        this.stepperService.updateFormError();

        if ( !this.contribution.isTabular ){
          this.extentForm = <FormGroup> this.contributionForm.get( `extent` );
          return this.contextService.getContext$( this.contribution.map ).pipe(
            finalize(() =>{
              setTimeout(() => {
                if ( !this.canDisplayMap ){
                  this.loggerService.errorOnToast$( $localize`:@@contextNotLoad:Erreur pendant le chargement du context` );
                }
              }, 250 );
            }),
          );
        } else {
          return of( null );
        }

      }),
    ).subscribe({
      next: ( context ) => {
        if ( context ) {
          this.coreService.setContext( context );
          this.canDisplayMap = true;
        }
      },
      error:    ( err ) => console.error( err ),
    });
  }

  /**
   * update pour forcé l'update du tableau de résultat
   */
  updateFields(){
    if ( this.fieldForm ){
      this.fieldDisplayData = <ContributionField[]> this.fieldForm.getRawValue();
    }
  }

  updateExtent(){
    this.contextService.drawContextEtent(  this.map, <ContributionExtent> this.extentForm.value, <string> this.contribution.sourceSRS  );
  }

  nextStep(): void{
    this.stepperService.setStep$( StepType.GeneralInformation );
  }

  override ngOnDestroy(): void {
    super.ngOnDestroy();
    this.mapService.deleteMap( this.mapId );
  }
}
