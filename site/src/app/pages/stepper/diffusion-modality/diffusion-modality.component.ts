import { Component, OnInit } from '@angular/core';

import { BaseComponent } from '../../../core/components/base/base.component';
import { ContributionService } from '../../../core/services/contribution.service';
import { Contribution } from '../../../core/models/contribution';
import { takeUntil } from 'rxjs';
import { StepType } from '../../../core/models/step-type';
import { StepperService } from '../stepper.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector:    `alk-diffusion-modality`,
  templateUrl: `./diffusion-modality.component.html`,
  styleUrls:   [`./diffusion-modality.component.scss`],
})
export class DiffusionModalityComponent extends BaseComponent  implements OnInit   {

  public contribution: Contribution = null;

  public contributionForm: FormGroup = null;

  public subdomainsForm: FormControl = null;

  constructor(
    private contributionService: ContributionService,
    private stepperService: StepperService,
  ) {
    super(); /* empty */
  }

  ngOnInit(): void {
    this.contributionService.getContribtuion$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe({
        next:  ( contribution ) => {
          this.contribution = contribution;
          this.contributionForm = <FormGroup> this.contributionService.getContributionForm().controls[`DiffusionModality`];
          this.subdomainsForm = <FormControl> this.contributionForm.get( `subdomains` );

          ( <FormGroup> this.contributionService.getContributionForm().controls[`GeneralInformation`]).markAllAsTouched();
          ( <FormGroup> this.contributionService.getContributionForm().controls[`DataOverview`]).markAllAsTouched();
          this.stepperService.updateFormError();
        },
        error: console.error,
      });
  }

  nextStep(): void{
    this.stepperService.setStep$( StepType.SynthesisAndValidation );
  }

  previousStep(): void{
    this.stepperService.setStep$( StepType.GeneralInformation );
  }


}
