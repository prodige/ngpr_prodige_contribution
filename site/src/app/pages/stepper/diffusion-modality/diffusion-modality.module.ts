import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiffusionModalityComponent } from './diffusion-modality.component';
import { RubricModule } from '../../../core/components/rubric/rubric.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormErrorModule } from '../../../core/components/form-error/form-error.module';

@NgModule({
  imports: [
    CommonModule,
    RubricModule,
    FormsModule,
    ReactiveFormsModule,
    FormErrorModule,
  ],
  exports: [
    DiffusionModalityComponent,
  ],
  declarations: [
    DiffusionModalityComponent,
  ],
})
export class DiffusionModalityModule { }
