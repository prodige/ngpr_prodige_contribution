import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiffusionModalityComponent } from './diffusion-modality.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CommonModule} from '@angular/common';
import {RubricModule} from '../../../core/components/rubric/rubric.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

describe('DiffusionModalityComponent', () => {
  let component: DiffusionModalityComponent;
  let fixture: ComponentFixture<DiffusionModalityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiffusionModalityComponent ],
      imports: [ HttpClientTestingModule,
        CommonModule,
        RubricModule,
        FormsModule,
        ReactiveFormsModule,],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DiffusionModalityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
