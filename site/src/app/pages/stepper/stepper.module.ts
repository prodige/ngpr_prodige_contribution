import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StepperComponent } from './stepper.component';
import { StepperRoutingModule } from './stepper-routing.module';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { DataOverviewModule } from './data-overview/data-overview.module';
import { GeneralInformationModule } from './general-information/general-information.module';
import { DiffusionModalityModule } from './diffusion-modality/diffusion-modality.module';
import { SynthesisAndValidationModule } from './synthesis-and-validation/synthesis-and-validation.module';

@NgModule({
  imports: [
    CommonModule,
    NgbNavModule,

    StepperRoutingModule,
    DataOverviewModule,
    GeneralInformationModule,
    DiffusionModalityModule,
    SynthesisAndValidationModule,
  ],
  exports:      [],
  declarations: [
    StepperComponent,
  ],
})
export class StepperModule { }
