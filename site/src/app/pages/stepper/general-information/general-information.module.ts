import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralInformationComponent } from './general-information.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ContactModule } from '../../../core/components/contact/contact.module';
import { FormErrorModule } from '../../../core/components/form-error/form-error.module';



@NgModule({
  imports: [
    ContactModule,

    CommonModule,
    FormsModule,
    NgMultiSelectDropDownModule.forRoot(),
    ReactiveFormsModule,
    FormErrorModule,
  ],
  declarations: [
    GeneralInformationComponent,
  ],
  exports: [
    GeneralInformationComponent,
  ],
})
export class GeneralInformationModule { }
