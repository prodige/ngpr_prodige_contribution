import { Component, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs';
import { ContributionService } from '../../../core/services/contribution.service';
import { Contribution } from '../../../core/models/contribution';
import { BaseComponent } from '../../../core/components/base/base.component';
import { ProdigeThemeService } from '../../../core/services/prodige-theme.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ListItem } from 'ng-multiselect-dropdown/multiselect.model';
import { ThemeEnum } from '../../../core/models/thesarus';
import { StepType } from '../../../core/models/step-type';
import { StepperService } from '../stepper.service';
import { GEO_SCALE } from '../../../core/models/geo-scale';
import { USE_LIMITATION } from "../../../core/models/md-contrainte";
import { UPDATE_CYCLE } from '../../../core/models/update-cycle';
import { ReccordListKeyWord, SelectedKeyWord } from '../../../core/models/thesarus-record';
import * as moment from 'moment';
import { FormArray, FormGroup } from '@angular/forms';
import { LoggerService } from '../../../core/components/logger/logger.service';
import { LOGGER_MESSAGE } from '../../../core/models/logger-message';
import { ThematiqueIsoService } from "../../../core/services/thematiqueIso.service";
import { ThematiqueIso } from '../../../core/models/thematique-iso';
import { MdMaintenanceService } from "../../../core/services/mdMaintenance.service";
import { MdMaintenance } from "../../../core/models/md-maintenance";



@Component({
  selector:    `alk-general-information`,
  templateUrl: `./general-information.component.html`,
  styleUrls:   [`./general-information.component.scss`],
})
export class GeneralInformationComponent extends BaseComponent implements OnInit {
  public ThemeEnum = ThemeEnum;
  public contribution: Contribution;

  public geoScale = GEO_SCALE;

  /** Setting des sélection de tags */
  public dropdownSettings: IDropdownSettings = {};

  /** liste des cycle de mis à jour */
  public updateCycle = UPDATE_CYCLE;
  public updateCycleKey = Object.keys( UPDATE_CYCLE );


  /** liste des contraintes de la ressource */
  public mdContrainte = USE_LIMITATION;

  /** liste des catégories ISO */
  public thematiquesIso: ThematiqueIso[] = [];

  /** liste des fréquences de mise à jour de la ressource */
  public mdMaintenance: MdMaintenance[] = [];

  /** La liste des mot-clés */
  public themeKeyWords: ReccordListKeyWord = {};

  /** Les tags sélectionnée */
  public selectedThemes: SelectedKeyWord[] = [];
  public selectedPlace: SelectedKeyWord[] = [];

  /** */
  public contributionForm: FormGroup;
  public contactForm: FormArray;

  constructor(
    private contributionService: ContributionService,
    private prodigeThemeService: ProdigeThemeService,
    private thematiqueisoService: ThematiqueIsoService,
    private mdMaintenanceService : MdMaintenanceService,
    private stepperService: StepperService,
    private loggerService: LoggerService,
  ) {
    super();


  }

  ngOnInit(): void {
    this.themeKeyWords[ThemeEnum.place] = [];
    this.themeKeyWords[ThemeEnum.theme] = [];

    this.dropdownSettings = {
      singleSelection:       false,
      idField:               `id`,
      enableCheckAll:        false,
      textField:             `displayValue`,
      selectAllText:         $localize`:@@selectAll:Sélectionner tout`,
      unSelectAllText:       $localize`:@@unSelectAll:Tout désélectionner`,
      allowSearchFilter:     true,
      searchPlaceholderText: $localize`:@@toResearch:Rechercher`,
      allowRemoteDataSearch: true,
    };

    this.loadContribution();

    this.loadThesarus();

    this.loadThematiquesIso();

    this.loadMdMaintenance();
  }

  public onThemeSelect( item?: ListItem[]): void {
    console.log( this.selectedThemes, item );
    this.contributionForm.controls[`themeKeyword`].patchValue( this.prodigeThemeService.itemsToThesarus( this.selectedThemes ));
  }

  public onPlaceSelect(): void {
    this.contributionForm.controls[`localisationKeyword`].patchValue( this.prodigeThemeService.itemsToThesarus( this.selectedPlace ));
  }

  /**
   *  recherche des mot-clés
   */
  public onFilterChange( item: ListItem | string, type: ThemeEnum ): void {
    this.prodigeThemeService.autoCompleteKeyWord( item.toString(), type ).pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe({
        next:  ( themes ) => this.themeKeyWords[type] = themes,
        error: console.error,
      });
  }

  public addContact(): void {
    console.log( `addContact`, this.contributionService.buildContact());
    this.contactForm.push( this.contributionService.buildContact());
  }

  public removeContact( idx: number ): void {
    this.contactForm.removeAt( idx );
    if ( this.contactForm.length === 0 ) {
      this.addContact();
    }
  }

  public changeDate( dateFrom: 'createDate' | 'updateDate', dateTo: `updatedAt` | `createdAt` ) {
    const newDate = moment( <string> this.contributionForm.get( dateFrom ).value, this.contributionService.getDateFormat()).format();
    this.contributionForm.controls[dateTo].setValue( newDate );
  }

  nextStep(): void {
    this.stepperService.setStep$( StepType.DiffusionModality );
  }

  previousStep(): void {
    this.stepperService.setStep$( StepType.DataOverview );
  }

  public tableLowerCase() {
    this.contributionForm.patchValue({ table: ( <Contribution> this.contributionForm.value ).table.toLowerCase() });
  }

  private loadContribution() {
    this.contributionService.getContribtuion$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( contribution ) => {
        this.contribution = contribution;

        this.contributionForm = <FormGroup> this.contributionService.getContributionForm().controls[`GeneralInformation`];
        this.contactForm = <FormArray> this.contributionForm.get( `contact` );

        this.selectedPlace = this.prodigeThemeService.thesarusToItem(
          ( <Contribution> this.contributionForm.value )[`localisationKeyword`]);

        this.selectedThemes = this.prodigeThemeService.thesarusToItem(
          ( <Contribution> this.contributionForm.value )[`themeKeyword`]);

        ( <FormGroup> this.contributionService.getContributionForm().controls[`DataOverview`]).markAllAsTouched();
        this.stepperService.updateFormError();
        console.log( this.selectedPlace, this.selectedThemes, this.contribution.themeKeyword );
      });
  }

  private loadThesarus() {
    // préparation des requête pour l'autocompletion des theme et location
    this.prodigeThemeService.loadThesaurus().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe({
        next: () => {
          /** Recherche par défaut pour ne avoir de problème avec la déselection de tag */
          this.onFilterChange( `a`, ThemeEnum.theme );
          this.onFilterChange( `a`, ThemeEnum.place );
        },
        error: ( err ) => {
          this.loggerService.errorOnToast$( LOGGER_MESSAGE.thesarusLoadError );
          console.error( err );
        },
      });
  }


  private loadThematiquesIso() {
    this.thematiqueisoService.getThematiquesIso$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( thematiqueIso ) => this.thematiquesIso = thematiqueIso );
  }

  private loadMdMaintenance() {
    this.mdMaintenanceService.getMdMaintenance$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( mdMaintenance ) => this.mdMaintenance = mdMaintenance );
  }
}
