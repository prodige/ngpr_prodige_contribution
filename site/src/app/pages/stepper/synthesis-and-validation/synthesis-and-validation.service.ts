import { Injectable } from '@angular/core';
import {
  Contribution,
  CONTRIBUTION_KEY_PATCH, CONTRIBUTION_KEY_PATCH_TABULAIRE,
  contributionValidator,
} from '../../../core/models/contribution';
import { HttpClient } from '@angular/common/http';
import { EnvService } from '../../../core/services/env/env.service';
import { map, Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: `root`,
})
export class SynthesisAndValidationService {
  private headerMerge = { 'Accept': `application/ld+json`, 'Content-Type':  `application/merge-patch+json` };

  constructor(
    private httpClient: HttpClient,
    private envService: EnvService,
  ) { }

  sendContribution( contribution: Contribution, contribForm: FormGroup ): Observable<Contribution> {
    const sendData: {[key:string]: unknown} = {};

    Object.values( <{[key: string]: unknown}>contribForm.getRawValue()).forEach(( control )=>{
      Object.entries( control ).forEach(([ key, value ])=> {
        contribution[key as keyof Contribution] = <never>value;
      });
    });

    console.log( contribution );

    if ( contribution.isTabular ){
      CONTRIBUTION_KEY_PATCH_TABULAIRE.forEach(( key ) => ( sendData[key] = contribution[key as keyof Contribution]));
    }
    else {
      CONTRIBUTION_KEY_PATCH.forEach(( key ) => ( sendData[key] = contribution[key as keyof Contribution]));
    }


    if ( sendData[`fields`]){
      sendData[`fields`] = ( <Contribution>sendData )[`fields`]
        .filter(( field ) => field.selected )
        .map(( field ) => {
          delete field.selected;
          return field;
      });
    }

    sendData[`status`] = `api/statuses/4`;
    console.log( sendData );

    return this.httpClient.patch<Contribution>( `${this.envService.urlContribution}/api/data/${contribution.id}`, sendData,
      { withCredentials: true, headers: this.headerMerge });
  }
}
