import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { StepType } from '../../../core/models/step-type';
import { StepperService } from '../stepper.service';
import { ContributionService } from '../../../core/services/contribution.service';
import { BaseComponent } from '../../../core/components/base/base.component';
import { Contribution, ContributionField } from '../../../core/models/contribution';
import { switchMap, takeUntil } from 'rxjs';
import { UPDATE_CYCLE } from '../../../core/models/update-cycle';
import { ProdigeThemeService } from '../../../core/services/prodige-theme.service';
import { SelectedKeyWord } from '../../../core/models/thesarus-record';
import { RoleService } from '../../../core/services/role.service';
import { RoleCode } from '../../../core/models/role-code';
import { MdMaintenance } from '../../../core/models/md-maintenance';
import { RubricService } from '../../../core/components/rubric/rubric.service';
import { ITree } from '../../../core/components/tree-view/i-tree';
import { Domain } from '../../../core/models/domain';
import { MapIdService, MapService, VisualiseurCoreService } from '@alkante/visualiseur-core';
import { ContextService } from '../../../core/services/context.service';
import { Map } from 'ol';
import { SynthesisAndValidationService } from './synthesis-and-validation.service';
import { AbstractControl, FormArray, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoggerService } from '../../../core/components/logger/logger.service';
import { ModalEndPopinComponent } from '../../../core/modals/modal-end-popin/modal-end-popin.component';
import { PRODIGE_TYPE } from '../../../core/models/prodige-type';
import { Contact } from '../../../core/models/contact';
import { MdMaintenanceService } from "../../../core/services/mdMaintenance.service";
import { ThematiqueIsoService } from "../../../core/services/thematiqueIso.service";
import { ThematiqueIso } from "../../../core/models/thematique-iso";

@Component({
  selector:    `alk-synthesis-and-validation`,
  templateUrl: `./synthesis-and-validation.component.html`,
  styleUrls:   [`./synthesis-and-validation.component.scss`],
})
export class SynthesisAndValidationComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild( `modalEndPopin` ) modalEndPopin: ModalEndPopinComponent;
  public updateCyle = UPDATE_CYCLE;

  public contribution: Contribution = null;

  public roleCodes: RoleCode[] = [];

  public mdMaintenance: MdMaintenance[] = [];

  public thematiquesIso: ThematiqueIso[] = [];

  public domainTree: ITree<Domain>[];

  public canDisplayMap = false;

  /** Les tags sélectionnée */
  public selectedThemes: SelectedKeyWord[] = [];
  public selectedPlace: SelectedKeyWord[] = [];

  public contributionForm: FormGroup;

  public contribControl: { [key: string]: AbstractControl; } = {};

  public contacts: Contact[];
  public fieldForm: FormArray;

  public fieldDisplayData: ContributionField[];

  public isValid: boolean = false;

  public prodigeType = PRODIGE_TYPE;

  private map: Map;

  private mapId = `visualiseur`;

  constructor(
    private stepperService: StepperService,
    private contributionService: ContributionService,
    private prodigeThemeService: ProdigeThemeService,
    private roleService: RoleService,
    private rubricService: RubricService,
    private contextService: ContextService,
    private coreService: VisualiseurCoreService,
    private mapService: MapService,
    private mapIdService: MapIdService,
    private synthesisService: SynthesisAndValidationService,
    private ngbModal: NgbModal,
    private loggerService: LoggerService,
    private mdMaintenanceService: MdMaintenanceService,
    private thematiqueisoService: ThematiqueIsoService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.loadContribution();

    this.loadRoles();

    this.loadMdMaintenance();

    this.loadThematiquesIso();
  }

  private loadContribution() {
    this.mapIdService.setId( this.mapId || `main` );
    this.mapService.setMapId( `visualiseur`, this.mapId || `main` );

    this.mapService.getMapAfterComplete( this.mapId ).pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( map ) => {
        this.map = map;

        this.contextService.drawContextEtent( map, this.contribution.extent, <string> this.contribution.sourceSRS );
      });

    this.contributionService.getContribtuion$().pipe(
      takeUntil( this.endSubscriptions ),
      switchMap(( contribution ) => {
        this.contribution = contribution;

        this.contributionForm = this.contributionService.getContributionForm();
        Object.values( this.contributionForm.controls ).forEach(( controls ) => {
          this.contribControl = Object.assign( this.contribControl, ( <FormGroup>controls ).controls );
        });
        console.log( this.contribControl );
        this.contacts = <Contact[]>( <FormArray> this.contribControl[`contact`]).getRawValue();
        this.fieldForm = <FormArray> this.contribControl[`fields`];

        this.fieldDisplayData = <ContributionField[]> this.fieldForm.getRawValue();

        this.selectedPlace = this.prodigeThemeService.thesarusToItem(
          ( <Contribution> this.contributionForm.controls[`GeneralInformation`].value )[`localisationKeyword`]);

        this.selectedThemes = this.prodigeThemeService.thesarusToItem(
          ( <Contribution> this.contributionForm.controls[`GeneralInformation`].value )[`themeKeyword`]);

        this.contributionForm.markAllAsTouched();
        this.isValid = this.contributionForm.valid;

        if ( !this.isValid ) {
          const sectionFormDae = document.querySelector( `#topScrollError` );
          if ( sectionFormDae ) {
            sectionFormDae.scrollIntoView();
          }
        }

        this.stepperService.updateFormError();

        return this.rubricService.getDomainTree$();
      }),
      switchMap(( domainTree ) => {
        this.domainTree = domainTree;
        this.rubricService.setSelectedSubDomain( <number[]> this.contribControl[`subdomains`].value );
        return this.contextService.getContext$( this.contribution.map );
      }),
    ).subscribe(( context ) => {
      this.coreService.setContext( context );
      this.canDisplayMap = true;

    });

  }

  public onMapShow() {
    this.contextService.updateShowExtent( this.map );
  }

  public sendData() {
    this.isLoading = true;
    this.synthesisService.sendContribution( this.contribution, this.contributionForm ).pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe({
        next: ( contribution ) => {
          this.modalEndPopin.open( contribution.metadataUuid, this.contribution.isTabular );
          this.isLoading = false;
        },
        error: ( err ) => {
          this.loggerService.errorOnModal$( $localize`:@@synthesisValidationError:Une erreur est survenue pendant la sauvegarde` );
          this.isLoading = false;
          console.error( err );
        },
      });
  }

  private loadRoles() {
    this.roleService.getRoles$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( roleCodes ) => this.roleCodes = roleCodes );
  }

  private loadMdMaintenance() {
    this.mdMaintenanceService.getMdMaintenance$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( mdMaintenance ) => this.mdMaintenance = mdMaintenance );
  }

  private loadThematiquesIso() {
    this.thematiqueisoService.getThematiquesIso$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( thematiqueIso ) => this.thematiquesIso = thematiqueIso );
  }

  previousStep(): void {
    this.stepperService.setStep$( StepType.DiffusionModality );
  }

  override ngOnDestroy(): void {
    super.ngOnDestroy();
    this.mapService.deleteMap( this.mapId );
  }
}
