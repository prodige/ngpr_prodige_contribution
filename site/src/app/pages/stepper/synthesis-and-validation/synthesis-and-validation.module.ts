import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SynthesisAndValidationComponent } from './synthesis-and-validation.component';
import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { ScaleToLabelModule } from '../../../core/pipes/scale-to-label/scale-to-label.module';
import { ContrainteToLabelModule } from '../../../core/pipes/contrainte-to-label/contrainte-to-label.module';
import { RoleToLabelModule } from '../../../core/pipes/role-to-label/role-to-label.module';
import { FieldDisplayModule } from '../../../core/components/field-display/field-display.module';
import { DomainsToLabelModule } from '../../../core/pipes/domains-to-label/domains-to-label.module';
import { MapModule } from '@alkante/visualiseur-core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormErrorModule } from '../../../core/components/form-error/form-error.module';
import { ModalEndPopinModule } from '../../../core/modals/modal-end-popin/modal-end-popin.module';
import { FieldFilterPipeModule } from '../../../core/pipes/field-filter-pipe/field-filter-pipe.module';
import { MaintenanceToLabelModule } from "../../../core/pipes/maintenace-to-label/maintenance-to-label.module";
import { ThematiqueToLabelModule } from "../../../core/pipes/thematique-to-label/thematique-to-label.module";

@NgModule({
    imports: [
        CommonModule,
        NgbAccordionModule,
        ScaleToLabelModule,
        ContrainteToLabelModule,
        MaintenanceToLabelModule,
        RoleToLabelModule,
        FieldDisplayModule,
        DomainsToLabelModule,
        MapModule,
        ReactiveFormsModule,
        FormErrorModule,
        ModalEndPopinModule,
        FieldFilterPipeModule,
        ThematiqueToLabelModule,
    ],
  exports:      [
    SynthesisAndValidationComponent,
  ],
  declarations: [
    SynthesisAndValidationComponent,
  ],
})
export class SynthesisAndValidationModule { }
