import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StepperComponent } from './stepper.component';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CommonModule} from '@angular/common';
import {NgbNavModule} from '@ng-bootstrap/ng-bootstrap';
import {StepperRoutingModule} from './stepper-routing.module';
import {DataOverviewModule} from './data-overview/data-overview.module';
import {GeneralInformationModule} from './general-information/general-information.module';
import {DiffusionModalityModule} from './diffusion-modality/diffusion-modality.module';
import {SynthesisAndValidationModule} from './synthesis-and-validation/synthesis-and-validation.module';

describe('StepperComponent', () => {
  let component: StepperComponent;
  let fixture: ComponentFixture<StepperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StepperComponent ],
      imports:[RouterTestingModule, HttpClientTestingModule,    CommonModule,
        NgbNavModule,

        StepperRoutingModule,
        DataOverviewModule,
        GeneralInformationModule,
        DiffusionModalityModule,
        SynthesisAndValidationModule,]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
