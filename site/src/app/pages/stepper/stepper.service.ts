import { Injectable } from '@angular/core';
import { StepType } from '../../core/models/step-type';
import { BehaviorSubject, Observable } from 'rxjs';
import { AbstractControl } from '@angular/forms';

@Injectable({
  providedIn: `root`,
})
export class StepperService {
  private step$ = new BehaviorSubject<StepType>( StepType.DataOverview );
  private formError$ = new BehaviorSubject<{[key: string]: boolean}>({});

  private formError: {[key: string]: boolean} = {};

  private controls: {[p: string]: AbstractControl} = {};

  public setStep$( step: StepType ): void {
    this.step$.next( step );
  }

  public getStep$(): Observable<StepType> {
    return this.step$.asObservable();
  }

  public getFormError$(): Observable<{[key: string]: boolean}>{
    return this.formError$.asObservable();
  }

  public setControl( controls: {[p: string]: AbstractControl}){
    this.formError = {};
    this.controls = controls;
    this.updateFormError();
  }

  public updateFormError(): void{
    Object.entries( this.controls ).forEach(([ key, form ]) =>{
      this.formError[key] = form.invalid && ( form.touched || form.dirty );
    });

    this.formError$.next( this.formError );
  }


}
