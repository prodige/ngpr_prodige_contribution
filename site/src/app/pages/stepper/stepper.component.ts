import { Component, OnInit } from '@angular/core';
import { StepType } from '../../core/models/step-type';
import { BaseComponent } from '../../core/components/base/base.component';
import { StepperService } from './stepper.service';
import { takeUntil } from 'rxjs';
import { ContributionService } from '../../core/services/contribution.service';
import { Router } from '@angular/router';

@Component({
  selector:    `alk-stepper`,
  templateUrl: `./stepper.component.html`,
  styleUrls:   [`./stepper.component.scss`],
})
export class StepperComponent extends BaseComponent implements OnInit{
  // pour utiliser l'enum StepType dans le template
  public StepType = StepType;

  // pour savoir quel étape affiché
  public step: StepType = StepType.DataOverview;

  public formError: {[key: string]: boolean} = {};

  constructor(
    private router: Router,
    private stepperService: StepperService,
    private contributionService: ContributionService,
  ) {
    super();

    /** écoute des mise à jour du stepper **/
    this.stepperService.getStep$().pipe(
        takeUntil( this.endSubscriptions ),
      )
      .subscribe(( step ) => this.step = step );

    /** écoute des mise à jour des erreur de formulaire */
    this.stepperService.getFormError$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( formError ) => this.formError = formError );
  }

  ngOnInit(): void {
    this.contributionService.getContribtuion$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(() =>{
        const controls = this.contributionService.getContributionForm().controls;
        this.stepperService.setControl( controls );
      });

  }


}
