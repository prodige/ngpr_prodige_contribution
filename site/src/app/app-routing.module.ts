import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CasGuard } from './core/services/cas/cas.guard';
import { ContributionResolver } from './core/services/contribution.resolver';

const routes: Routes = [
  {
    path:         `contribute/home`,
    pathMatch:    `full`,
    canActivate:  [CasGuard],
    loadChildren: () => import( `./pages/home/home.module` ).then(({ HomeModule }) => HomeModule ),
  },  {
    path:         `contribute/home/:uuid`,
    pathMatch:    `full`,
    canActivate:  [CasGuard],
    loadChildren: () => import( `./pages/home/home.module` ).then(({ HomeModule }) => HomeModule ),
    resolve:      { contribution: ContributionResolver },
  }, {
    path:         `contribute/form/:id`,
    canActivate:  [CasGuard],
    loadChildren: () => import( `./pages/stepper/stepper.module` ).then(({ StepperModule }) => StepperModule ),
    resolve:      [ContributionResolver],
  },
  {
    path:       `**`,
    redirectTo: `contribute/home`,
    pathMatch:  `full`,
  },
];

@NgModule({
  imports: [RouterModule.forRoot( routes )],
  exports: [RouterModule],
})
export class AppRoutingModule { }
