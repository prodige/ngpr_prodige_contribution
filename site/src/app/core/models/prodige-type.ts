export type ProdigeType = {[key: string]: string}
export const PRODIGE_TYPE: ProdigeType = {
  STRING:  $localize`:@@Text:Texte`,
  NUMBER:  $localize`:@@Digital:Numérique`,
  BOOLEAN: $localize`:@@Boolean:Booléen`,
  DATE:    $localize`:@@Date:Date`,
  INTEGER: $localize`:@@Integer:Entier`,
};



