import { z } from 'zod';


export const geoFeatureValidator = z.object({
  properties: z.record( z.unknown()),
});

export const geoCollectionValidator = z.object({
  features: geoFeatureValidator.array(),
});

export type GeoFeature = z.infer<typeof geoFeatureValidator>;
export type GeoCollection = z.infer<typeof geoCollectionValidator>;

