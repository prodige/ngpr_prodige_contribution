import { z } from 'zod';

export const contactValidator = z.object({
  organization: z.string().optional().nullable(),
  mail:         z.string().optional().nullable(),
  role:         z.string().optional().nullable(),
  type:         z.string().optional().nullable(),
});

export type Contact = z.infer<typeof contactValidator>;
