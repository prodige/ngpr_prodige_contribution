import { z } from 'zod';

export const userValidator = z.object({
  login:     z.string(),
  name:      z.string(),
  firstName: z.string(),
});

export type User = z.infer<typeof userValidator>;
