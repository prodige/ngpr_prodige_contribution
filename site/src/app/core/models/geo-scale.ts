export const GEO_SCALE = [{
  label: `1:1 000`,
  value: 1000,
}, {
  label: `1:5 000`,
  value: 5000,
}, {
  label: `1:10 000`,
  value: 10000,
}, {
  label: `1:25 000`,
  value: 25000,
}, {
  label: `1:50 000`,
  value: 50000,
}, {
  label: `1:100 000`,
  value: 100000,
}, {
  label: `1:200 000`,
  value: 200000,
}, {
  label: `1:300 000`,
  value: 300000,
}, {
  label: `1:500 000`,
  value: 500000,
}, {
  label: `1:1 000 000`,
  value: 1000000,
}];
