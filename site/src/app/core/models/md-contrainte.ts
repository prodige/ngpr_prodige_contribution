export const USE_LIMITATION = [{
  label: `Aucune condition ne s’applique`,
  value: `Aucune condition ne s’applique`,
}, {
  label: `Conditions inconnues`,
  value: `Conditions inconnues`,
}, {
  label: `Utilisation libre sous réserve de mentionner la source (a minima le nom du producteur) et la date de sa dernière mise à jour`,
  value: `Utilisation libre sous réserve de mentionner la source (a minima le nom du producteur) et la date de sa dernière mise à jour`,
}, {
  label: `Données ouvertes`,
  value: `Données ouvertes`,
}];
