import { z } from 'zod';

export const prodigeLayerValidator = z.object({
  storagePath: z.string(),
});

export const prodigeLayerApiValidator = z.object({
  'hydra:member': prodigeLayerValidator.array(),
});

export type ProdigeLayer = z.infer<typeof prodigeLayerValidator>;
export type ProdigeLayerApi = z.infer<typeof prodigeLayerApiValidator>;
