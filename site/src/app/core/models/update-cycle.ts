export type UpdateCycleType = {[key: string]: string}

export const UPDATE_CYCLE: UpdateCycleType = {
  historicalArchive: $localize`:@@Archive:Archivé`,
  required:          $localize`:@@CreationOrUpdateRequired:Création ou mise à jour requise`,
  underDevelopment:  $localize`:@@BeingCreated:En cours de création`,
  completed:         $localize`:@@Finalized:Finalisé`,
  onGoing:           $localize`:@@ContinuousUpdate:Mise à jour continue`,
  obsolete:          $localize`:@@Obsolete:Obsolète`,
  planned:           $localize`:@@Planned:Planifié`,
};
