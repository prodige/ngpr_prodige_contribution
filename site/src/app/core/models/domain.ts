import { z } from 'zod';

const parent = z.object({
  "@id": z.string(),
});

export const subDoaminValidator = z.object({
  id:   z.number(),
  name: z.string(),
});

export const domainValidator = z.object({
  "@id":         z.string(),
  uri:           z.string().optional(),
  id:            z.number(),
  name:          z.string(),
  type:          z.string().optional(),
  domain:        parent.optional(),
  rubric:        parent.optional(),
  parentUri:     z.string().optional(),
  subDoamins:    subDoaminValidator.array().optional(),
  domainSelects: subDoaminValidator.array().optional(),
});

export const domainApiValidator = z.object({
  "hydra:member": domainValidator.array(),
});

export type Domain = z.infer<typeof domainValidator>;
export type DomainApi = z.infer<typeof domainApiValidator>;

export type SubDoamin = z.infer<typeof subDoaminValidator>

export type DomanApiName = `rubrics` | `domains` | `subdomains`;
