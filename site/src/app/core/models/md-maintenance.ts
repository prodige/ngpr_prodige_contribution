import { z } from 'zod';

export const mdMaintenanceValidator = z.object({
  code:  z.string(),
  label: z.string(),
});

export const mdMaintenanceValidatorApi = z.object({
  entry: mdMaintenanceValidator.array(),
});

export type MdMaintenance = z.infer<typeof mdMaintenanceValidator>;
export type MdMaintenanceApi = z.infer<typeof mdMaintenanceValidatorApi>;
