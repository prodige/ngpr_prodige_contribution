import { z } from 'zod';

export const thematiqueIsoValidator = z.object({
  code:  z.string(),
  label: z.string(),
});

export const thematiqueIsoValidatorApi = z.object({
  entry: thematiqueIsoValidator.array(),
});

export type ThematiqueIso = z.infer<typeof thematiqueIsoValidator>;
export type ThematiqueIsoApi = z.infer<typeof thematiqueIsoValidatorApi>;
