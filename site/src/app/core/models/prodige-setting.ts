import { z } from 'zod';

export const prodigeSettingValidator = z.object({
  'system/site/name': z.string(),
});

export type ProdigeSetting = z.infer<typeof prodigeSettingValidator>;
