
export interface DataFile {
  contentUrl: string;
  id: number;
  data: string; // uri vers contribution
}
