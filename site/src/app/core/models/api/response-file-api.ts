import { z } from 'zod';

export const responseFileApiValidator = z.object({
  id:         z.number(),
  contentUrl: z.string(),
  name:       z.string(),
  error:      z.string().optional(),
});

export type ResponseFileApi = z.infer<typeof responseFileApiValidator>;
