import { z } from "zod";

export const casConnectApiValidator = z.object({
  success:   z.boolean(),
  connected: z.boolean(),
  login:     z.string(),
});

export type CasConnectApi = z.infer<typeof casConnectApiValidator>;
