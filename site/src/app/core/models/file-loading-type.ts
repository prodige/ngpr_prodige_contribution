export type FileLoadingType =  `no` | `loading` | `success` | `error`;

export interface IFileLoading  {
  [key:string]: FileLoadingType,
}
