import { z } from 'zod';

export const roleCodeValidator = z.object({
  code:  z.string(),
  label: z.string(),
});

export const roleCodeValidatorApi = z.object({
  entry: roleCodeValidator.array(),
});

export type RoleCode = z.infer<typeof roleCodeValidator>;
export type RoleCodeApi = z.infer<typeof roleCodeValidatorApi>;
