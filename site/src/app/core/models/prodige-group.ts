import { z } from 'zod';

export const prodigeGroupValidator = z.object({
  name: z.string(),
});

export const prodigeGroupeApiValidator = prodigeGroupValidator.array();

export type ProdigeGroup = z.infer<typeof prodigeGroupValidator>;
export type ProdigeGroupApi = z.infer<typeof prodigeGroupeApiValidator>;



