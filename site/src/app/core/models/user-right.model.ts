export interface UserRightApi{
  right: UserRight;
}

export interface UserRight {
  userNom: string;
  userPrenom: string;
  userLogin: string;
  userId: 1;
  userEmail: string;
  MANAGED_SUBDOMAINS: ManagedSubdomains[]
}

export interface ManagedSubdomains{
  id: number;
  name: string
}
