export const LOGGER_MESSAGE = {
  roleLoadError:          $localize`:@@roleLoadError:Erreur survenue pendant le chargement des rôles`,
  thematiqueIsoLoadError: $localize`:@@thematiqueIsoLoadError:Erreur survenue pendant le chargement des catégories Iso`,
  mdMaintenanceLoadError: $localize`:@@mdMaintenanceLoadError:Erreur survenue pendant le chargement des fréquence de mise à jours`,
  thesarusLoadError:      $localize`:@@thesarusLoadError:Erreur survenue pendant le chargement des Thésaurus`,
  contributionError:      $localize`:@@contributionErrorLoad:Erreur survenue pendant le chargement des données`,
  initializeFileError:    $localize`:@@contributionFileError:Erreur survenue pendant la création de votre contribution`,
  deleteFileError:        $localize`:@@contributionFileError:Erreur survenue pendant la suppression du fichier`,
  contributionNotFound:   $localize`:@@contributionNotFound:Votre contribution n'a pas été trouvée`,
  contributionBadStatus:  $localize`:@@contributionBadSatus:Erreur de statut sur votre contribution`,
  adminTableName:         $localize`:@@adminTableName:Erreur survenue pendant la recherche du nom de table`,
};
