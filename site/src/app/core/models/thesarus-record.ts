import { ThesarusKeyWord } from './thesarus';

export interface SelectedKeyWord {
  id: string;
  displayValue: string;
}

export interface ReccordKeyWord{
  [key:string]: string[];
}

export interface ReccordListKeyWord{
  [key:string]: ThesarusKeyWord[];
}
