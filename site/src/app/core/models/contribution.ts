import { RefinementCtx, z } from "zod";
import * as moment from 'moment';
import { contactValidator } from './contact';


// Extent
export const contributionExtentValidator = z.object({
  xmin: z.number(),
  ymin: z.number(),
  xmax: z.number(),
  ymax: z.number(),
});

// Fields
export const contributionFieldValidaor = z.object({
  name:        z.string(),
  type:        z.string(),
  alias:       z.string().optional(),
  description: z.string().optional(),
  selected:    z.boolean().optional(),
});

function transformDate( val: string, ctx: RefinementCtx ): string {
  const date = new Date( val );

  if ( !date ) {
    ctx.addIssue({
      code:    z.ZodIssueCode.custom,
      message: `NOT DATE VALID`,
    });

    return z.NEVER;
  }
  console.log( `transformDate`, moment( date ).format( `YYYY-MM-DD` ));
  return moment( date ).format( `YYYY-MM-DD` );
}

// Contribution
export const contributionValidator = z.object({
  id:                  z.number(),
  userId:              z.number(),
  admincartoDataId:    z.number(),
  map:                 z.string().optional(),
  datafiles:           z.string().array().optional(),
  layerName:           z.string().optional(),
  metadataUuid:        z.string().nullable().optional(),
  sourceSRS:           z.union([ z.string(), z.number() ]).optional(),
  extent:              contributionExtentValidator.optional(),
  sourceEncoding:      z.string().nullable().optional(),
  fields:              contributionFieldValidaor.array().optional(),
  table:               z.string().optional(),
  title:               z.string().nullable().optional(),
  abstract:            z.string().nullable().optional(),
  lineage:             z.string().nullable().optional(),
  createdAt:           z.string().optional().transform( transformDate ),
  createDate:          z.string().optional(),
  updatedAt:           z.string().optional().transform( transformDate ),
  updateDate:          z.string().optional(),
  themeKeyword:        z.record( z.string().array()).optional(),
  localisationKeyword: z.record( z.string().array()).optional(),
  equivalentScale:     z.number().optional(),
  geonetworkStatus:    z.string().optional(),
  subdomains:          z.number().array().optional(),
  openData:            z.boolean().optional(),
  contact:             contactValidator.array().optional(),
  type:                z.string().optional(),
  isTabular:           z.boolean().optional(),
  status:              z.string(),
  mdContrainte:        z.string().nullable().optional(),
  thematiqueISO:       z.string().nullable().optional(),
  mdMaintenance:       z.string().nullable().optional(),

});

export const contributionApiValidator = z.object({
  "hydra:member": contributionValidator.array(),
});

export type ContributionExtent = z.infer<typeof contributionExtentValidator>;
export type ContributionField = z.infer<typeof contributionFieldValidaor>;
export type Contribution = z.infer<typeof contributionValidator>;
export type ContribtuionApi = z.infer<typeof contributionApiValidator>;

export const CONTRIBUTION_KEY_PATCH = [
  `status`,
  `fields`,
  `table`,
  `title`,
  `abstract`,
  `lineage`,
  `thematiqueISO`,
  `mdContrainte`,
  `mdMaintenance`,
  `localisationKeyword`,
  `themeKeyword`,
  `equivalentScale`,
  `contact`,
  `geonetworkStatus`,
  `openData`,
  `subdomains`,
  `createdAt`,
  `updatedAt`,
  `extent`,
  `type`,
  `layerId`,
];
export const CONTRIBUTION_KEY_PATCH_TABULAIRE = [
  `status`,
  `fields`,
  `table`,
  `title`,
  `abstract`,
  `lineage`,
  `thematiqueISO`,
  `mdContrainte`,
  `mdMaintenance`,
  `localisationKeyword`,
  `themeKeyword`,
  `contact`,
  `geonetworkStatus`,
  `openData`,
  `subdomains`,
  `createdAt`,
  `updatedAt`,
  `type`,
];


export const CONTRIBUTION_STATUS = {
  dataCreated:   `/api/statuses/1`,
  fileCompleted: `/api/statuses/2`,
  created:       `/api/statuses/3`,
  edited:        `/api/statuses/4`,
};



