export enum StepType{
  DataOverview = 1,
  GeneralInformation = 2,
  DiffusionModality = 3,
  SynthesisAndValidation = 4
}
