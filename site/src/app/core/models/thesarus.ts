import { z } from "zod";

export const thesarusValidator = z.object({
  dname: z.string(),
  key:   z.string(),
});

export const thesarusApiValidator = thesarusValidator.array().array();

export const thesarusKeyWordValidator = z.object({
  values:       z.record( z.string()),
  definitions:  z.record( z.string()),
  thesaurusKey: z.string(),
  displayValue: z.record( z.string()).optional(),
  displayDef:   z.record( z.string()).optional(),
  id:           z.string().optional(),
});

export const thesarusKeyWordApiValidator = thesarusKeyWordValidator.array();

export type Thesarus = z.infer<typeof thesarusValidator>;
export type ThesarusApi = z.infer<typeof thesarusApiValidator>;

export type ThesarusKeyWord = z.infer<typeof thesarusKeyWordValidator>;
export type ThesarusKeyWordApi = z.infer<typeof thesarusKeyWordApiValidator>;

export enum ThemeEnum {
  theme = `theme`,
  place = `place`
}
