import { Directive, EventEmitter, HostListener, Output } from '@angular/core';

@Directive({
  selector: `[alkDragDropFile]`,
})
export class DragDropFileDirective {
  @Output() files = new  EventEmitter<FileList> ();

  constructor() { }

  @HostListener( `dragover`, [`$event`]) public onDragOver( evt: DragEvent ) {
    evt.preventDefault();
    evt.stopPropagation();
  }

  @HostListener( `dragleave`, [`$event`]) public onDragLeave( evt: DragEvent ) {
    evt.preventDefault();
    evt.stopPropagation();
  }

  @HostListener( `drop`, [`$event`]) public onDrop( evt: DragEvent ) {
    evt.preventDefault();
    evt.stopPropagation();

    if ( evt?.dataTransfer?.files?.length > 0 ) {
      this.files.emit( evt.dataTransfer.files );
    }
  }

}
