import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot, Router,
} from '@angular/router';
import { catchError, Observable, of, switchMap, throwError } from 'rxjs';
import { Contribution, CONTRIBUTION_STATUS } from '../models/contribution';
import { ContributionService } from './contribution.service';
import { LoggerService } from '../components/logger/logger.service';
import { LOGGER_MESSAGE } from '../models/logger-message';

@Injectable({
  providedIn: `root`,
})
export class ContributionResolver implements Resolve<Contribution|null> {
  constructor(
    private contributionService: ContributionService,
    private loggerService: LoggerService,
    private router: Router,
  )
  {}

  resolve( route: ActivatedRouteSnapshot ): Observable<Contribution|null> {
    const contribtuonId = route.paramMap.get( `id` );
    if ( contribtuonId ){
      return this.contributionService.loadContributionById( parseInt( contribtuonId, 10 )).pipe(
        switchMap(( contribtuon ) => {
          if ( contribtuon.status === CONTRIBUTION_STATUS.dataCreated || contribtuon.status === CONTRIBUTION_STATUS.fileCompleted ){
            return throwError(() => `Bad status` );
          }

          return of( contribtuon );
        }),
        catchError(( err ) => this.handleError( route, err as string )),
      );
    }

    const uuid = route.paramMap.get( `uuid` );
    console.log( ` UUID `, uuid );
    if ( uuid ){
      return this.contributionService.loadContributionByUuid( uuid ).pipe(
        switchMap(( contribtuon ) => {
          if ( !contribtuon ){
            this.loggerService.errorOnModal$( LOGGER_MESSAGE.contributionNotFound );
            return throwError(() => `Contribution not found` );
          }

          if ( contribtuon.status === CONTRIBUTION_STATUS.dataCreated ){
            this.loggerService.errorOnModal$( LOGGER_MESSAGE.contributionBadStatus );
            return throwError(() => `Bad status` );
          }

          return of( contribtuon );
        }),
        catchError(( err ) => this.handleError( route, err as string )),
      );
    }

    return this.handleError( route, `Contribution ID NULL` );
  }

  handleError( route: ActivatedRouteSnapshot, errorResponse: string ): Observable<null> {
    console.error( errorResponse );

    this.router.navigate([`/home`]).catch( console.error );

    return of( null );
  }
}
