import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  ThemeEnum,
  Thesarus,
  ThesarusApi,
  thesarusApiValidator,
  ThesarusKeyWordApi,
  thesarusKeyWordApiValidator,
} from '../models/thesarus';
import { map, Observable, of, switchMap, throwError } from 'rxjs';
import { EnvService } from './env/env.service';
import { ReccordKeyWord, SelectedKeyWord } from '../models/thesarus-record';

@Injectable({
  providedIn: `root`,
})
export class ProdigeThemeService {
  private language = `fre`;

  private keyExclude = [`external.theme.prodige`];

  private thesarusTheme: {[key: string]: string } = {};

  constructor(
    private clientHttp: HttpClient,
    private envService: EnvService,
  ) { }

  public loadThesaurus():Observable<boolean>{
    if ( Object.values( this.thesarusTheme ).length > 0 ){
      return of( true );
    }

    return this.clientHttp.get<ThesarusApi>( `${this.envService.catalogueUrl}/geonetwork/srv/fre/thesaurus?_content_type=json` ).pipe(
      map(( thesarusApi ) => thesarusApiValidator.parse( thesarusApi )),
      switchMap(( thesarusApi ) => this.prepareKey( thesarusApi )),
      map(( thesaruKeys ) => !!thesaruKeys ),
    );
  }

  public autoCompleteKeyWord( keyWord: string, type: ThemeEnum, rows = 5 ):Observable<ThesarusKeyWordApi>{

    if ( !this.thesarusTheme[type]){
      return of([] as ThesarusKeyWordApi );
    }

    return this.clientHttp.get<ThesarusKeyWordApi>(
      `${this.envService.catalogueUrl}/geonetwork/srv/api/registries/vocabularies/search?type=CONTAINS`
      + `&thesaurus=${this.thesarusTheme[type]}&rows=${rows}&q=${keyWord}&uri=*QUERY*&lang=fre`,
    ).pipe(
      map(( thesarusKeyWordApi ) => thesarusKeyWordApiValidator.parse( thesarusKeyWordApi )),
      map(( thesarusKeyWordApi ) => thesarusKeyWordApi.map(( thesarusKeyWord ) =>
        Object.assign( thesarusKeyWord, {
          displayValue: `${thesarusKeyWord.values[this.language]}`,
          displayDef:   thesarusKeyWord.definitions[this.language],
          id:           `${thesarusKeyWord.values[this.language]}::${thesarusKeyWord.thesaurusKey}`,
        }),
      )),
    );
  }

  private prepareKey( thesarusApi: ThesarusApi ): Observable<boolean>{
    if ( thesarusApi.length === 0 ){
      return throwError(() => `thesarusApi empty ` );
    }

    const thesarusList = thesarusApi[0];

    for ( const i in ThemeEnum ){
      this.prepareTheme( i as ThemeEnum, thesarusList );
    }

    return of( true );
  }


  prepareTheme( theme: ThemeEnum, thesarusList: Thesarus[]){
    this.thesarusTheme[theme] = thesarusList
      .filter(( thesarus ) => ( thesarus.dname === theme ) && this.keyExclude.indexOf( thesarus.key ) === -1 )
      .map(( thesarus ) => thesarus.key )
      .join( `,` );
  }

  /**
   * sépare les items ng-multiselect-dropdown dans les thesarus assorcier
   */
  public itemsToThesarus( selectedKeyWord: SelectedKeyWord[]): ReccordKeyWord {
    const keyWords: ReccordKeyWord = {};

    selectedKeyWord.forEach(( selectedTheme ) => {

      const idSplit = selectedTheme.id.split( `::` );
      if ( idSplit.length < 2 ){
        return false;
      }

      const id = idSplit[1];

      if ( !keyWords[id]){
        keyWords[id] = [];
      }
      keyWords[id].push( selectedTheme.displayValue );

      return true;
    });

    return keyWords;
  }

  /**
   * transform les thesarus en items pour ng-multiselect-dropdown
   * @private
   */
  public thesarusToItem( reccordKeyWords: ReccordKeyWord ): SelectedKeyWord[]{
    console.log( `thesarusToItem `, reccordKeyWords );
    if ( !reccordKeyWords ){
      return [];
    }
    let retour :SelectedKeyWord[] = [];

    Object.entries( reccordKeyWords ).forEach(([ key, reccordKeyWord ])=> {
      const tmpItem = reccordKeyWord.map(( keyWord ) => ({
        id:           `${keyWord}::${key}`,
        displayValue: keyWord,
      }));

      retour = retour.concat( tmpItem );
    });

    return retour;
  }
}
