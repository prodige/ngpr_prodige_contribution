import { Injectable } from '@angular/core';
import { GetDataService } from './get-data.service';
import { RoleCode, RoleCodeApi, roleCodeValidatorApi } from '../models/role-code';
import { HttpClient } from '@angular/common/http';
import { EnvService } from './env/env.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LoggerService } from '../components/logger/logger.service';
import { LOGGER_MESSAGE } from '../models/logger-message';

@Injectable({
  providedIn: `root`,
})
export class RoleService extends GetDataService<RoleCodeApi>{
  constructor(
    protected override httpClient: HttpClient,
    private envService: EnvService,
    protected override loggerService: LoggerService,
  ) {
    super( httpClient, loggerService );
  }

  getRoles$(): Observable<RoleCode[]> {
    const url = `${this.envService.catalogueUrl}/geonetwork/srv/api/standards/iso19139/codelists/roleCode/details?_content_type=json`;

    return super.getData$( url, roleCodeValidatorApi, LOGGER_MESSAGE.roleLoadError ).pipe(
      map(( result ) => result.entry ),
    );
  }
}
