import { Injectable } from '@angular/core';
import { GetDataService } from './get-data.service';
import { FeatureCollection } from '@alkante/visualiseur-core/lib/models/context';
import { HttpClient } from '@angular/common/http';
import { EnvService } from './env/env.service';
import { map, Observable } from 'rxjs';
import { Feature, Map } from 'ol';
import { ContributionExtent } from '../models/contribution';
import { Coordinate } from 'ol/coordinate';
import Polygon from 'ol/geom/Polygon';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import { transformExtent } from 'ol/proj';

@Injectable({
  providedIn: `root`,
})
export class ContextService extends GetDataService<FeatureCollection>{
  private featureExtent: Feature<Polygon>;

  private layer: VectorLayer<VectorSource>;

  constructor(
    protected override httpClient: HttpClient,
    protected envService: EnvService,
  ) {
    super( httpClient );
  }

  getContext$( mapFile: string ): Observable<FeatureCollection>{
    // on supprime l'ancienne feature à la création d'une carte
    this.featureExtent = null;

    const url = `${this.envService.serverUrl}/carto/context?account=1&contextPath=/${mapFile}`;

    // eslint-disable-next-line max-len
    // const url = `https://carto-test.atlasante.fr/carto/context?account=1&contextPath=/layers/3f719270-f589-460b-bf2c-6f75e856ae23.map&extent=-614973,5053097,1191092,6637194&callback=ng_jsonp_callback_4`;

    return this.getData$( url, null, null, null, true );
  }

  drawContextEtent( map: Map, extent: ContributionExtent, projection: string ): void {
    console.log( 43,  projection );
    if ( !map ) {
      return;
    }
    const extentTab = [ extent.xmin, extent.ymin, extent.xmax, extent.ymax ];
    const extentToMap = projection ? transformExtent( extentTab, projection, map.getView().getProjection()) : extentTab;
    const coordinates: Coordinate[] = [
      [ extentToMap[0], extentToMap[1] ],
      [ extentToMap[2], extentToMap[1] ],
      [ extentToMap[2], extentToMap[3] ],
      [ extentToMap[0], extentToMap[3] ],
      [ extentToMap[0], extentToMap[1] ],
    ];

    console.log( extentTab, extentToMap, coordinates );

    if ( !this.featureExtent ) {
      this.featureExtent = new Feature({
        geometry: new Polygon([coordinates]),
      });

      const vectorSource = new VectorSource({
        wrapX:    false,
        features: [this.featureExtent],
      });

      this.layer = new VectorLayer({
        source: vectorSource,
      });

      this.layer.setMap( map );
    } else {
      this.featureExtent.setGeometry( new Polygon([coordinates]));
    }


    map.getView().fit( extentToMap );

  }

  public updateShowExtent( map: Map ): void{
    if ( this.layer ){
      this.layer.setMap( map );
    }
  }
}
