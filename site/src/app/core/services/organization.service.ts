import { Injectable } from '@angular/core';
import { GetDataService } from './get-data.service';
import { ProdigeGroupApi, prodigeGroupeApiValidator } from '../models/prodige-group';
import { HttpClient } from '@angular/common/http';
import { Observable, OperatorFunction } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { EnvService } from './env/env.service';

@Injectable({
  providedIn: `root`,
})
export class OrganizationService extends GetDataService<ProdigeGroupApi> {

  private organization: string[] = [];

  constructor(
    protected override httpClient: HttpClient,
    private envService: EnvService,
  ) {
    super( httpClient );
  }

  loadOrganization$(): Observable<boolean> {
    // this.searchTest();
    const url = `${this.envService.catalogueUrl}/geonetwork/srv/api/groups?`;

    return super.getData$( url, prodigeGroupeApiValidator ).pipe(
      map(( result ) => {
        this.organization = result.map(( prodigeGroupe ) => prodigeGroupe.name );
        return true;
      }),
    );
  }

  search: OperatorFunction<string, readonly string[]> = ( text$: Observable<string> ) =>
    text$.pipe(
      debounceTime( 200 ),
      distinctUntilChanged(),
      map(( term ) =>
        term.length < 1 ? [] : this.organization.filter(( v ) => v.toLowerCase().indexOf( term.toLowerCase()) > -1 ).slice( 0, 10 ),
      ),
    );

  searchTest() {
    /* const body = {
     "aggregations":  { "groupPublished": { "terms": { "field": ``, "size": 10 }}},
     "from":         0,
     "size":         20,
        "query":            { "function_score": { "query": { "bool": { "must": [{ "terms": { "isTemplate": [`s`]}}, { "terms": { "root": [`gmd:CI_ResponsibleParty`]}}]}}}},
     "_source": { "includes": [ `id`, `uuid`, `creat*`, `group*`, `resource*`, `owner*` ]},
   };*/
    /* const body = {
      "from":             0,
      "size":             20,
      "sort":             [{ "resourceTitleObject.default.keyword": `asc` }, `_score` ],
      "query":            { "function_score": { "query": { "bool": { "must": [{ "terms": { "isTemplate": [`s`]}}, { "terms": { "root": [`gmd:CI_ResponsibleParty`]}}]}}}},
      "aggregations":     { "groupPublished": { "terms": { "field": `groupPublished`, "size": 10 }}},
      "_source":          { "includes": [ `id`, `uuid`, `creat*`, `group*`, `resource*`, `owner*` ]},
      "track_total_hits": true,
    };*/





    const body = {
      "from":         0,
      "size":         20,
      "sort":         [{ "resourceTitleObject.default.keyword": `asc` }, `_score` ],
      "query":        { "function_score": { "query": { "bool": { "must": [{ "terms": { "isTemplate": [`s`]}}]}}}},
      "aggregations": {
        "valid":          { "terms": { "field": `valid`, "size": 10 }},
        "groupOwner":     { "terms": { "field": `groupOwner`, "size": 10 }},
        "recordOwner":    { "terms": { "field": `recordOwner`, "size": 10 }},
        "groupPublished": { "terms": { "field": `groupPublished`, "size": 10 }},
        "isHarvested":    { "terms": { "field": `isHarvested`, "size": 2 }},
      },
      "_source":          { "includes": [ `id`, `uuid`, `creat*`, `group*`, `resource*`, `owner*`, `recordOwner`, `status*`, `isTemplate`, `valid`, `isHarvested`, `changeDate`, `documentStandard` ]},
      "track_total_hits": true,
    };
  this.httpClient.post( `${this.envService.catalogueUrl}/geonetwork/srv/api/search/records/_search`, body, { withCredentials: true })


      .subscribe(( result ) => {
        console.log( result );
      });

  }
}
