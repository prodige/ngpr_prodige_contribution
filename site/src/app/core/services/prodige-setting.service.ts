import { Injectable } from '@angular/core';
import { GetDataService } from './get-data.service';
import { ProdigeSetting, prodigeSettingValidator } from '../models/prodige-setting';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { EnvService } from './env/env.service';

@Injectable({
  providedIn: `root`,
})
export class ProdigeSettingService extends GetDataService<ProdigeSetting>  {

  constructor(
    protected override httpClient: HttpClient,
    private envService: EnvService,
  ) {
    super( httpClient );
  }

  getPlateformName(): Observable<string>{
    const url =  `${this.envService.catalogueUrl}/geonetwork/srv/api/site/settings`;

    return this.getData$( url, prodigeSettingValidator ).pipe(
      map(( result ) => result[`system/site/name`]),
    );
  }

}
