import { Injectable } from '@angular/core';
import { map, Observable, of } from 'rxjs';
import { User, userValidator } from '../models/user';
import { GetDataService } from './get-data.service';
import { HttpClient } from '@angular/common/http';
import { EnvService } from './env/env.service';
import { ManagedSubdomains, UserRight, UserRightApi } from '../models/user-right.model';

@Injectable({
  providedIn: `root`,
})
export class UserService extends GetDataService<User>{
  private headerGet = { 'Accept': `application/ld+json`, 'Content-Type': `application/ld+json`, withCredentials: true };

  private subDomains: ManagedSubdomains[] = null;

  constructor(
    private envService: EnvService,
    protected override httpClient: HttpClient,
  ) {
    super( httpClient );
  }

  getUser$(): Observable<User>{
    const url = `${this.envService.urlAdmin}/api/internal/me`;

    return this.getData$( url, userValidator, null, this.headerGet );
  }

  getSubDomains$(): Observable<ManagedSubdomains[]>{
    if ( this.subDomains ){
      return of( this.subDomains );
    }

    return this.httpClient.get<UserRightApi>( `${this.envService.urlAdmin}/api/internal/verify_right?TRAITEMENTS=GLOBAL_RIGHTS`, { withCredentials: true })
      .pipe(
        map(( result ) =>{
          console.log( `result ? `, result, result?.right?.MANAGED_SUBDOMAINS?.length );
          if ( result?.right?.MANAGED_SUBDOMAINS?.length ){
            this.subDomains = result.right?.MANAGED_SUBDOMAINS;
            return result.right?.MANAGED_SUBDOMAINS;
          }
          return [];
        }),
      );
  }
}
