import { TestBed } from '@angular/core/testing';

import { CasGuard } from './cas.guard';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CasService} from './cas.service';

describe('CasGuard', () => {
  let guard: CasGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ CasService ]
    });
    guard = TestBed.inject(CasGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
