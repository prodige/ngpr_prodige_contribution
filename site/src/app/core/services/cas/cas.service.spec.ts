import { TestBed } from '@angular/core/testing';

import { CasService } from './cas.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('CasService', () => {
  let service: CasService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
    });
    service = TestBed.inject(CasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
