import { Injectable } from '@angular/core';
import { GetDataService } from './get-data.service';
import { MdMaintenance, MdMaintenanceApi, mdMaintenanceValidatorApi } from '../models/md-maintenance';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { EnvService } from './env/env.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LoggerService } from '../components/logger/logger.service';
import { LOGGER_MESSAGE } from '../models/logger-message';

@Injectable({
  providedIn: `root`,
})
export class MdMaintenanceService extends GetDataService<MdMaintenanceApi> {

  constructor(
    protected override httpClient: HttpClient,
    private envService: EnvService,
    protected override loggerService: LoggerService,
  ) {
    super( httpClient, loggerService );
  }

  getMdMaintenance$(): Observable<MdMaintenance[]> {
    const url = `${this.envService.catalogueUrl}/geonetwork/srv/api/standards/iso19139/codelists/gmd:MD_MaintenanceFrequencyCode/details?_content_type=json`;

    return super.getData$( url, mdMaintenanceValidatorApi, LOGGER_MESSAGE.mdMaintenanceLoadError, { headers: new HttpHeaders({ 'Accept-Language': `fre` }) }).pipe(
      map(( result ) => result.entry ),
    );

  }
}
