import { Injectable } from '@angular/core';
import { GetDataService } from './get-data.service';
import { ThematiqueIso, ThematiqueIsoApi, thematiqueIsoValidatorApi } from '../models/thematique-iso';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { EnvService } from './env/env.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LoggerService } from '../components/logger/logger.service';
import { LOGGER_MESSAGE } from '../models/logger-message';

@Injectable({
  providedIn: `root`,
})
export class ThematiqueIsoService extends GetDataService<ThematiqueIsoApi> {
  constructor(
    protected override httpClient: HttpClient,
    private envService: EnvService,
    protected override loggerService: LoggerService,
  ) {
    super( httpClient, loggerService );
  }

  getThematiquesIso$(): Observable<ThematiqueIso[]> {
    const url = `${this.envService.catalogueUrl}/geonetwork/srv/api/standards/iso19139/codelists/topicCategory/details?_content_type=json`;

    return super.getData$( url, thematiqueIsoValidatorApi, LOGGER_MESSAGE.thematiqueIsoLoadError,{ headers: new HttpHeaders({ 'Accept-Language': `fre` }) } ).pipe(
      map(( result ) => result.entry ),
    );

  }
}
