import { Injectable } from '@angular/core';
import {
  ContribtuionApi,
  Contribution, CONTRIBUTION_STATUS,
  ContributionField,
  contributionValidator,
} from '../models/contribution';
import { AsyncSubject, catchError, map, Observable, of, ReplaySubject, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { EnvService } from './env/env.service';
import * as moment from 'moment';
import {
  AbstractControl, AsyncValidatorFn,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { LoggerService } from '../components/logger/logger.service';
import { LOGGER_MESSAGE } from '../models/logger-message';
import { ProdigeLayer, ProdigeLayerApi, prodigeLayerApiValidator } from '../models/prodige-layer';
import { UserService } from './user.service';
import { Contact } from '../models/contact';

@Injectable({
  providedIn: `root`,
})
export class ContributionService {
  private headerGet = { 'Accept': `application/ld+json`, 'Content-Type': `application/ld+json` };
  private headerGetCollection = { 'Accept': `application/ld+json` };
  private contribution!: Contribution;
  private contribution$ = new ReplaySubject<Contribution>( 1 ); // bufferSize à 1 pour avoir seulement la dernière valeur

  private dateFormat = `YYYY-MM-DD`;

  private contributionForm: FormGroup;

  // TODO refaire au propre le systeme de nom unique
  private oldTableNameResult$: AsyncSubject<ValidationErrors|null> = null;

  private oldSearchTableName: string = null;

  private oldSearchTableResult:ValidationErrors = null;

  constructor(
    private httpClient: HttpClient,
    private envService: EnvService,
    private fb: FormBuilder,
    private loggerService: LoggerService,
    private userService: UserService,
  ) { /* empty */ }

  public loadContributionById( contributionId: number ): Observable<Contribution>{
    return this.httpClient.get<Contribution>(
      `${this.envService.urlContribution}/api/data/${contributionId}`, { withCredentials: true, headers: this.headerGet },
    )
      .pipe(
        map(( contribution ) =>{
          if ( !( contribution.contact instanceof Array ) && contribution.contact ){
            contribution.contact = [<Contact>contribution.contact];
          }

          this.setContribution( contribution );
          return contribution;
        }),
        map(( contribution ) => {
          this.contribution.fields = this.contribution.fields.map<ContributionField>(( field ) => {
            field.selected = true;

            field.alias = field.alias ? field.alias : ``;
            field.description = field.description ? field.description : ``;

            return field;
          });

          if ( this.contribution.table && this.contribution.table.toLowerCase().indexOf( `tmp_` ) === 0 ){
            this.contribution.table = ``;
          }

          this.contribution.createDate = this.transformDate( this.contribution.createdAt );
          this.contribution.updateDate = this.transformDate( this.contribution.updatedAt );

          this.contribution.openData = !!this.contribution.openData;

          if ( typeof this.contribution.sourceSRS === `number` ){
            this.contribution.sourceSRS = `EPSG:${this.contribution.sourceSRS}`;
          }


          this.contribution.subdomains =  this.contribution.subdomains ?  this.contribution.subdomains : [];

          if ( !this.contribution.themeKeyword || this.contribution.themeKeyword instanceof Array ){
            this.contribution.themeKeyword = null;
          }

          if ( !this.contribution.localisationKeyword || this.contribution.localisationKeyword instanceof Array ) {
            this.contribution.localisationKeyword = null;
          }

          this.contribution.isTabular = this.contribution.type === `/api/lex_layer_types/3`;

          this.initFormControl();

          return contribution;
        }),
        catchError(( err ) => {
          console.error( err );
          this.loggerService.errorOnModal$( LOGGER_MESSAGE.contributionError );
          return throwError(() => <unknown>err );
        }),

      );

  }

  public loadContributionByUuid( uuid: string ): Observable<Contribution>{
    return this.httpClient.get<ContribtuionApi>(
      `${this.envService.urlContribution}/api/data?page=1&itemsPage=1&metadataUuid=${uuid}`, { withCredentials: true, headers: this.headerGetCollection },
    )
      .pipe(
        map(( contributions )=> {
          if ( !( contributions instanceof  Object )){
            return null;
          }

          const contribution =  contributions[`hydra:member`][0];

          if ( !contribution.contact?.length && contribution.contact ){
            contribution.contact = [<Contact>contribution.contact];
          }

          this.setContribution( contribution );
          return contributionValidator.parse( contribution );
        }),
        catchError(( err ) => {
          console.error( err );
          this.loggerService.errorOnModal$( LOGGER_MESSAGE.contributionError );
          return throwError(() => <unknown>err );
        }),

      );

  }

  public getContribtuion$(): Observable<Contribution>{
    return this.contribution$.asObservable();
  }

  public setContribution( contribution: Contribution ) {
    this.contribution = contribution;
    this.contribution$.next( contribution );
  }

  public getDateFormat(): string{
    return this.dateFormat;
  }

  public getContributionForm(){

    return this.contributionForm;
  }

  private transformDate( val: string ): string{
    const date = new Date( val );

    if ( !date ){ return val }

    return moment( date ).format( `YYYY-MM-DD` );
  }

  private initFormControl(){
    const extentControl = [
      Validators.required,
      Validators.pattern( /\d+/ ),
    ];

    const  dataOverviewForm: {[key:string]: AbstractControl, extent?: FormGroup} = {
      'sourceEncoding': new FormControl( this.contribution.sourceEncoding, Validators.required ),
      'fields':         new FormArray(
      this.contribution.fields.map(( field ) => (
        new FormGroup({
          'name':        new FormControl( field.name ),
          'alias':       new FormControl( field.alias ),
          'description': new FormControl( field.description ),
          'type':        new FormControl( field.type ),
          'selected':    new FormControl( field.selected ),
        })
      )),
    ) };

    if ( !this.contribution.isTabular ){
      dataOverviewForm.extent  = new FormGroup({
        'xmin': new FormControl( this.contribution.extent.xmin, extentControl ),
        'ymin': new FormControl( this.contribution.extent.ymin, extentControl ),
        'xmax': new FormControl( this.contribution.extent.xmax, extentControl ),
        'ymax': new FormControl( this.contribution.extent.ymax, extentControl ),
      });
    }

    const generalInformationForm:  {[key:string]: AbstractControl, equivalentScale?: FormControl} = {
      'table':          new FormControl({
          value:    this.contribution.table,
          disabled: this.contribution.status === CONTRIBUTION_STATUS.edited,
        }, {
        validators: [
          Validators.required,
          Validators.pattern( /^[a-z0-9_]+$/ ),
          this.uniqTable(),
        ],
        asyncValidators: [
          this.uniqTableApi(),
        ],
        updateOn: `blur`,
      }),
      'title':               new FormControl( this.contribution.title, [Validators.required]),
      'abstract':            new FormControl( this.contribution.abstract, [Validators.required]),
      'lineage':             new FormControl( this.contribution.lineage ),
      'themeKeyword':        new FormControl( this.contribution.themeKeyword ),
      'localisationKeyword': new FormControl( this.contribution.localisationKeyword ),
      'updatedAt':           new FormControl( this.contribution.updatedAt, [Validators.required]),
      'createdAt':           new FormControl( this.contribution.createdAt, [Validators.required]),
      'updateDate':          new FormControl( this.contribution.updateDate, [Validators.required]),
      'createDate':          new FormControl( this.contribution.createDate, [Validators.required]),
      'thematiqueISO':       new FormControl( this.contribution.thematiqueISO ),
      'mdMaintenance':       new FormControl( this.contribution.mdMaintenance ),
      'mdContrainte':        new FormControl( this.contribution.mdContrainte ),
      'geonetworkStatus':    new FormControl( this.contribution.geonetworkStatus ),
      'contact':             new FormArray( this.getContactForm()),
    };

    if ( !this.contribution.isTabular ){
      generalInformationForm.equivalentScale = new FormControl( this.contribution.equivalentScale, [Validators.required]);
    }


    this.contributionForm = new FormGroup({
      DataOverview:       new FormGroup( dataOverviewForm ),
      GeneralInformation: new FormGroup( generalInformationForm ),
      DiffusionModality:  new FormGroup({
        'openData':   new FormControl( this.contribution.openData ),
        'subdomains': new FormControl( this.contribution.subdomains, {
          validators:      [Validators.required],
          asyncValidators: [
            this.checkDomain(),
          ],
          updateOn: `blur`,
        }),
      }),
    });
  }

  public buildContact(): FormGroup{
    return this.fb.group({
      'organization': [ ``, [Validators.required]],
      'mail':         [ ``, [ Validators.email, Validators.required ]],
      'role':         [ null, [Validators.required]],
      'type':         [ `text`, [Validators.required]],
    });
  }

  private getContactForm(): FormGroup[]{
    if ( !( this.contribution.contact instanceof Array ) || this.contribution.contact.length === 0 ){
      return [this.buildContact()];
    }

    return this.contribution.contact.map(( contact ) => {
      const isLinkContact = contact.type === `link`;
      return this.fb.group({
        'organization': [{ value: contact.organization, disabled: isLinkContact }, Validators.required ],
        'mail':         [{ value: contact.mail, disabled: isLinkContact }, [ Validators.email, Validators.required ]],
        'role':         [ contact.role, Validators.required ],
        'type':         [ contact.type, Validators.required ],
      });

    });
  }

  private uniqTable(): ValidatorFn {
    return ( control: AbstractControl ): ValidationErrors | null => {
      const tableName = ( <string>control.value ).toLowerCase();
      if ( tableName === `table` ) {
        return  { badTableName: { value: <string>control.value }};
      }
      else if ( tableName.indexOf( `tmp_` ) === 0 ){
        return { tmpError: { value: <string>control.value }};
      }

      return null;
    };
  }

  private uniqTableApi(): AsyncValidatorFn {
    return ( control: AbstractControl ): Observable<ValidationErrors | null> => {
      const tableName = ( <string>control.value ).toLowerCase();

      if ( this.oldSearchTableName === tableName ){
        return of( this.oldSearchTableResult );
      }

      this.oldSearchTableName = tableName;
      console.log( `uniqTableApi` );

      if ( this.oldTableNameResult$ && this.oldTableNameResult$.observed ){
        return this.oldTableNameResult$.asObservable();
      }

      this.oldTableNameResult$ = new AsyncSubject<ValidationErrors | null>();
      this.checkTableName( tableName ).pipe(
        map(( layer ) =>{
          if ( layer && layer.storagePath && layer.storagePath === tableName ){
            return { uniqTableName: { value: layer.storagePath }};
          }
          return null;
        }),
        catchError(( err ) => {
          console.error( err );
          this.loggerService.errorOnToast$( LOGGER_MESSAGE.adminTableName );
          return null;
        }),
      )
        .subscribe(( result ) =>{
          this.oldTableNameResult$.next( result );
          this.oldTableNameResult$.complete();
        });

      return this.oldTableNameResult$.asObservable();

    };
  }

  private checkTableName( name: string ): Observable<ProdigeLayer>{
    return this.httpClient.get<ProdigeLayerApi>(
      `${this.envService.urlAdmin}/api/layers?api/layers?page=1&itemsPage=1&properties[]=storagePath&storagePath=${name}`,
      { withCredentials: true, headers: this.headerGetCollection },
    ).pipe(
      map(( layers ) => prodigeLayerApiValidator.parse( layers )),
      map(( layers ) => {
        if ( !layers[`hydra:member`].length ){
          return null;
        }
        return layers[`hydra:member`][0];
      }),
    );
  }

  private checkDomain(): AsyncValidatorFn{
    return ( control: AbstractControl ): Observable<ValidationErrors | null> => {
      const subDomains = <number[]>control.value;
      if ( !subDomains?.length ){
        return null;
      }

      return this.userService.getSubDomains$().pipe(
        map(( managedSubdomains ) =>{
          const result = subDomains.find(( subDomain ) =>
            managedSubdomains.find(( managedSubdomain ) => managedSubdomain.id === subDomain ),
          );

          console.log( !!result, result );
          if ( !result ){
            return { checkDomain: { value: !result }};
          }
          return null;

        }),
      );
    };
  }

}
