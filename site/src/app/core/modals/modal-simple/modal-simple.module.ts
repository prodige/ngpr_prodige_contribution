import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalSimpleComponent } from './modal-simple.component';



@NgModule({
  declarations: [
    ModalSimpleComponent,
  ],
  exports: [
    ModalSimpleComponent,
  ],
  imports: [
    CommonModule,
  ],
})
export class ModalSimpleModule { }
