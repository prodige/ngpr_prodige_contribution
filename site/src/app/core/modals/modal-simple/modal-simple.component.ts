import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component } from '@angular/core';

@Component({
  selector:    `alk-modal-simple`,
  templateUrl: `./modal-simple.component.html`,
  styleUrls:   [`./modal-simple.component.scss`],
})
export class ModalSimpleComponent {

  public message: string  = null;

  private title: string  = null;

  private isSuccess: boolean;

  constructor(
    public activeModal: NgbActiveModal,
  ) { }

  public setMessage( newValue: string ) {
    if ( newValue ) {
      this.message = newValue;
    }
  }

  public setIsSuccessModal( isSuccess: boolean ) {
    this.isSuccess = isSuccess;
    if ( !this.message ) {
      if ( this.isSuccess ) {
        this.message = `La donnée a été publiée.`;
      } else {
        this.message = `Une erreur est survenue.`;
      }
    }
  }

  public setTitle( newValue: string ) {
    if ( newValue ) {
      this.title = newValue;
    }
  }

  public closeModal() {
    console.log( `Close modal` );

    // On ferme simplement la modal.
    this.activeModal.close();

  }

}
