import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSimpleComponent } from './modal-simple.component';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';

describe('ModalSimpleComponent', () => {
  let fixture: ComponentFixture<ModalSimpleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSimpleComponent ],
      imports: [NgbModule],
      providers: [ NgbActiveModal]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSimpleComponent);

  });

  it('should create', () => {
    return true;
  });
});
