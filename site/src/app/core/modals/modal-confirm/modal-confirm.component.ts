import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector:    `alk-modal-confirm`,
  templateUrl: `./modal-confirm.component.html`,
  styleUrls:   [`./modal-confirm.component.scss`],
})
export class ModalConfirmComponent {
  @Input() public message: string;

  constructor (
    public activeModal: NgbActiveModal,
  ) { }

  onClickCancel() {
    this.activeModal.close( false );
  }

  onClickDelete() {
    this.activeModal.close( true );
  }
}
