import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalEndPopinComponent } from './modal-end-popin.component';



@NgModule({
  declarations: [
    ModalEndPopinComponent,
  ],
  exports: [
    ModalEndPopinComponent,
  ],
  imports: [
    CommonModule,
  ],
})
export class ModalEndPopinModule { }
