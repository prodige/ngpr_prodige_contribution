import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEndPopinComponent } from './modal-end-popin.component';

describe('ModalEndPopinComponent', () => {
  let component: ModalEndPopinComponent;
  let fixture: ComponentFixture<ModalEndPopinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalEndPopinComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEndPopinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
