import { Component, ElementRef, ViewChild } from '@angular/core';
import { EnvService } from '../../services/env/env.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector:    `alk-modal-end-popin`,
  templateUrl: `./modal-end-popin.component.html`,
  styleUrls:   [`./modal-end-popin.component.scss`],
})
export class ModalEndPopinComponent {
  @ViewChild( `content` ) modalContent: ElementRef;

  private modalRef:  NgbModalRef = null;
  public url: {[key:string]: string} = {};

  public isTabular = false;

  constructor(
    private ngbModal: NgbModal,
    private envService: EnvService,
  ) {
    this.url[`importOtherDataset`] = `${this.envService.urlContribution}`;
    this.url[`contributionList`] = `${this.envService.catalogueUrl}/geonetwork/srv/fre/catalog.edit#/board?`
      + `sortBy=dateStamp&sortOrder=desc&isTemplate=%5B%22y%22,%22n%22%5D&resultType=manager&from=1&to=20`
      + `&query_string=%7B%22resourceType%22:%7B%22dataset%22:true%7D%7D&owner=40102`;
  }

  public open( uuid: string, isTabular: boolean ): void{
    this.isTabular = isTabular;
    /** */
    if ( !this.modalRef ){
      this.url[`expertMode`] = `${this.envService.catalogueUrl}/geonetwork/srv/fre/catalog.search#/metadata/${uuid}`;
      this.url[`defaultRepresentation`] = `${this.envService.admincartoUrl}/edit_map/${uuid}`;

      this.modalRef = this.ngbModal.open( this.modalContent );
    }

    this.modalRef.result.finally(() => this.modalRef = null );
  }
}
