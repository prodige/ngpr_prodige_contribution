import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FieldFilterPipe } from './field-filter.pipe';



@NgModule({
  declarations: [
    FieldFilterPipe,
  ],
  exports: [
    FieldFilterPipe,
  ],
  imports: [
    CommonModule,
  ],
})
export class FieldFilterPipeModule { }
