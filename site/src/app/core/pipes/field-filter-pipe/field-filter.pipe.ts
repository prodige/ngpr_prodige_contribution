import { Pipe, PipeTransform } from '@angular/core';
import { ContributionField } from '../../models/contribution';

@Pipe({
  name: `fieldFilter`,
})
export class FieldFilterPipe implements PipeTransform {

  transform( fields: ContributionField[]): ContributionField[] {
    return fields.filter(( field ) => field.selected );
  }

}
