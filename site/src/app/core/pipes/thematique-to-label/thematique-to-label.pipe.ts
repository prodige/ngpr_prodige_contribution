import { Pipe, PipeTransform } from '@angular/core';
import { ThematiqueIso } from '../../models/thematique-iso';


@Pipe({
  name: `thematiqueToLabel`,
})
export class ThematiqueToLabelPipe implements PipeTransform {

  transform( value: string, thematiques: ThematiqueIso[]) {
    const label = thematiques.find(( thematique ) => thematique.code === value );

    return label ? label.label : value;
  }

}
