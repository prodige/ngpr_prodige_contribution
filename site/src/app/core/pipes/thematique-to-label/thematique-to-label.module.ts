import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThematiqueToLabelPipe } from './thematique-to-label.pipe';

@NgModule({
  declarations: [
    ThematiqueToLabelPipe,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    ThematiqueToLabelPipe,
  ],
})
export class ThematiqueToLabelModule { }
