import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DomainsToLabelPipe } from './domains-to-label.pipe';



@NgModule({
  declarations: [
    DomainsToLabelPipe,
  ],
  exports: [
    DomainsToLabelPipe,
  ],
  imports: [
    CommonModule,
  ],
})
export class DomainsToLabelModule { }
