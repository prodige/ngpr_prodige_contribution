import { Pipe, PipeTransform } from '@angular/core';
import { Domain } from '../../models/domain';

@Pipe({
  name: `domainsToLabel`,
})
export class DomainsToLabelPipe implements PipeTransform {

  transform( domains: Domain[]): string {
    let label = ``;
    domains.forEach(( domain ) =>{
      label = `${label} ${( label ? `, ` : `` )} ${domain.name}`;
    });

    return label;
  }

}
