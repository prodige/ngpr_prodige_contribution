import { Pipe, PipeTransform } from '@angular/core';
import { RoleCode } from '../../models/role-code';

@Pipe({
  name: `roleToLabel`,
})
export class RoleToLabelPipe implements PipeTransform {

  transform( code: string, roles: RoleCode[]): string {
    const label = roles.find(( role ) => role.code === code );

    return label ? label.label : code;
  }

}
