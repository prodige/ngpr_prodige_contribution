import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleToLabelPipe } from './role-to-label.pipe';



@NgModule({
  declarations: [
    RoleToLabelPipe,
  ],
  exports: [
    RoleToLabelPipe,
  ],
  imports: [
    CommonModule,
  ],
})
export class RoleToLabelModule { }
