import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContrainteToLabelPipe } from './contrainte-to-label.pipe';

@NgModule({
  declarations: [
    ContrainteToLabelPipe,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    ContrainteToLabelPipe,
  ],
})
export class ContrainteToLabelModule { }
