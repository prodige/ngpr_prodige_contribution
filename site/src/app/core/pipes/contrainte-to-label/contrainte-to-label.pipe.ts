import { Pipe, PipeTransform } from '@angular/core';
import { USE_LIMITATION } from '../../models/md-contrainte';

@Pipe({
  name: `contrainteToLabel`,
})
export class ContrainteToLabelPipe implements PipeTransform {
  transform( value: string ): string {
    const label = USE_LIMITATION.find(( useLimitation ) => useLimitation.label === value );

    return label ? label.label : value;
  }
}
