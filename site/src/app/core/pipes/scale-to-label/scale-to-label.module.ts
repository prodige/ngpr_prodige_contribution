import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScaleToLabelPipe } from './scale-to-label.pipe';

@NgModule({
  declarations: [
    ScaleToLabelPipe,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    ScaleToLabelPipe,
  ],
})
export class ScaleToLabelModule { }
