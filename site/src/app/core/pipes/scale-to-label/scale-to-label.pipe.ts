import { Pipe, PipeTransform } from '@angular/core';
import { GEO_SCALE } from '../../models/geo-scale';

@Pipe({
  name: `scaleToLabel`,
})
export class ScaleToLabelPipe implements PipeTransform {
  transform( value: number ): string|number {
    const label = GEO_SCALE.find(( geoScale ) => geoScale.value === value );

    return label ? label.label : value;
  }
}
