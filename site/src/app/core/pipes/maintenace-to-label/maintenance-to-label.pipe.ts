import { Pipe, PipeTransform } from '@angular/core';
import { MdMaintenance } from '../../models/md-maintenance';


@Pipe({
  name: `maintenanceToLabel`,
})
export class MaintenanceToLabelPipe implements PipeTransform {

  transform( value: string, mdMaintenance: MdMaintenance[]) {
    const label = mdMaintenance.find(( maintenance ) => maintenance.code === value );

    return label ? label.label : value;
  }

}
