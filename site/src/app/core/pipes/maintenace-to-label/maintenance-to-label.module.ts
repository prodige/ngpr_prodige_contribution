import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaintenanceToLabelPipe } from './maintenance-to-label.pipe';

@NgModule({
  declarations: [
    MaintenanceToLabelPipe,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    MaintenanceToLabelPipe,
  ],
})
export class MaintenanceToLabelModule { }
