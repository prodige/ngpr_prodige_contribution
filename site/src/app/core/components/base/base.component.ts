import { Component, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { EnvService } from '../../services/env/env.service';

@Component({
  selector:    `alk-base`,
  templateUrl: `./base.component.html`,
  styleUrls:   [`./base.component.scss`],
})
export abstract class BaseComponent implements OnDestroy {

  /** Vrai si le composant est en cours de chargement (pour afficher le contenu html uniquement à la fin du ngOnInit par exemple) */
  public isLoading = false;
  /** Vrai si application est en mode debug (var d'env) */
  protected isInDebugMode = false;
  /** Subject émettant une valeur à la destruction du composant, à utiliser avec les pipes rxjs et **takeUntil(this.endSubscriptions)** sur les observable */
  protected endSubscriptions = new Subject<boolean>();
  /** En cas d'héritage multiple, permet aux enfants de préciser à leur parent de ne pas passer this.isLoading à **false** à la fin de leur *ngOnInit* */
  protected readonly keepLoadingAfterSuperInit: boolean = false;

  protected constructor() {
    this.isInDebugMode = EnvService.enableDebug ?? false;
  }

  ngOnDestroy(): void {
    this.endSubscriptions.next( true );
    this.endSubscriptions.complete();
  }

}
