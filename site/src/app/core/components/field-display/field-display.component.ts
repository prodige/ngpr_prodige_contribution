import { Component, Input, OnInit } from '@angular/core';
import { ContributionField } from '../../models/contribution';
import { GeoFeature } from '../../models/geojson-validator';
import { BaseComponent } from '../base/base.component';
import { FieldDisplayService } from './field-display.service';
import { finalize, takeUntil } from 'rxjs';
import { LoggerService } from '../logger/logger.service';

@Component({
  selector:    `alk-field-display`,
  templateUrl: `./field-display.component.html`,
  styleUrls:   [`./field-display.component.scss`],
})
export class FieldDisplayComponent extends BaseComponent implements OnInit {

  @Input() fields: ContributionField[];
  @Input() mapFile: string;
  @Input() layerId: string;
  @Input() withValue: boolean;
  @Input() contributionId: number;
  @Input() admincartoDataId: number;
  @Input() isTabular: boolean;

  public geoFeatures: GeoFeature[] = [];



  constructor(
    private fieldDisplayService: FieldDisplayService,
    private loggerService: LoggerService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.fieldDisplayService.getFeatures( this.mapFile, this.layerId, this.admincartoDataId, this.isTabular ).pipe(
      takeUntil( this.endSubscriptions ),
      finalize(() => {
        console.log( 35 );
        setTimeout(() => {
          if ( !this.geoFeatures || this.geoFeatures.length === 0 ){
            this.loggerService.errorOnToast$( $localize`:@@contextNotLoad:Erreur pendant le chargement du Tableau des données` );
          }
        }, 250 );
      }),
    )
      .subscribe(( geoFeature ) =>{
        console.log( `fieldDisplayService` );
        this.geoFeatures = geoFeature;
      });
  }
}
