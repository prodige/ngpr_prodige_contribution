import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldDisplayComponent } from './field-display.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('FieldDisplayComponent', () => {
  let component: FieldDisplayComponent;
  let fixture: ComponentFixture<FieldDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FieldDisplayComponent],
      imports: [HttpClientTestingModule],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
