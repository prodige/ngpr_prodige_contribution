import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FieldDisplayComponent } from './field-display.component';
import { FieldFilterPipeModule } from '../../pipes/field-filter-pipe/field-filter-pipe.module';

@NgModule({
  declarations: [
    FieldDisplayComponent,
  ],
  exports: [
    FieldDisplayComponent,
  ],
  imports: [
    FieldFilterPipeModule,
    CommonModule,
  ],
})
export class FieldDisplayModule { }
