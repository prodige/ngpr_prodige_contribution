import { Injectable } from '@angular/core';
import { map, Observable, throwError } from 'rxjs';
import { GeoCollection, geoCollectionValidator, GeoFeature } from '../../models/geojson-validator';
import { GetDataService } from '../../services/get-data.service';
import { HttpClient } from '@angular/common/http';
import { EnvService } from '../../services/env/env.service';

@Injectable({
  providedIn: `root`,
})
export class FieldDisplayService extends GetDataService<GeoCollection>{

  constructor(
    protected envService: EnvService,
    protected override httpClient: HttpClient,
  ) {
    super( httpClient );
  }

  public getFeatures( mapfile: string, layerId: string, admincartoDataId: number, isTabular: boolean ): Observable<GeoFeature[]> {

    let url =  null;

    let option = null;
    const dataType = isTabular ? 3 : 1;

    option = { withCredentials: true };
    url = `${this.envService.admincartoUrl}/api/contribution/data/preview/${admincartoDataId}/${dataType}`;

    console.log( isTabular, admincartoDataId, url, option );

    return this.getData$( url, geoCollectionValidator, null, option ).pipe(
      map(( geoCollection ) => geoCollection.features.slice( 0, 20 )),
    );
  }
}
