import { TestBed } from '@angular/core/testing';
import {FieldDisplayService} from './field-display.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';


describe('FieldDisplayService', () => {
  let service: FieldDisplayService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(FieldDisplayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
