import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ToastLoggerComponent } from './toast-logger/toast-logger.component';
import { ModalLoggerComponent } from './modal-logger/modal-logger.component';
import { NgbToastModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ModalLoggerComponent,
    ToastLoggerComponent,
  ],
  exports: [
    ModalLoggerComponent,
    ToastLoggerComponent,
  ],
  imports: [
    NgbToastModule,
    CommonModule,
    FormsModule,
  ],
})
export class LoggerModule { }
