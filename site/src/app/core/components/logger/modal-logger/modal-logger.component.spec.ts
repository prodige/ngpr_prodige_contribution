import { ComponentFixture, TestBed } from '@angular/core/testing';
import {ModalLoggerComponent} from './modal-logger.component';


describe('DisplayErrorComponent', () => {
  let component: ModalLoggerComponent;
  let fixture: ComponentFixture<ModalLoggerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalLoggerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalLoggerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
