import { Component, OnInit } from '@angular/core';
import { ProdigeSettingService } from '../../services/prodige-setting.service';
import { BaseComponent } from '../base/base.component';
import { takeUntil } from 'rxjs';

@Component({
  selector:    `alk-footer`,
  templateUrl: `./footer.component.html`,
  styleUrls:   [`./footer.component.scss`],
})
export class FooterComponent extends BaseComponent implements OnInit{
  public plateformName:string;

  constructor(
    private prodigeSettingService: ProdigeSettingService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.prodigeSettingService.getPlateformName().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( name )=> this.plateformName = name );
  }
}
