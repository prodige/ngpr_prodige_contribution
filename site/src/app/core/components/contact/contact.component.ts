import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import { OperatorFunction, takeUntil } from 'rxjs';
import { RoleCode } from '../../models/role-code';
import { EnvService } from '../../services/env/env.service';
import { OrganizationService } from '../../services/organization.service';
import { RoleService } from '../../services/role.service';
import { AbstractControl, FormGroup } from '@angular/forms';

@Component({
  selector:    `alk-contact`,
  templateUrl: `./contact.component.html`,
  styleUrls:   [`./contact.component.scss`],
})
export class ContactComponent extends BaseComponent implements OnInit{
  @Input()  contactForm!: FormGroup|AbstractControl;
  @Input()  total!: number;

  @Output() removeContact = new EventEmitter<void>();

  public roleCodes: RoleCode[] = [];

  public orgaAutoComplete: OperatorFunction<string, readonly string[]>;

  public formGroup:FormGroup = null;

  constructor(
    private envService: EnvService,
    private organizationService: OrganizationService,
    private roleService: RoleService,
  ) {
    super();

    this.orgaAutoComplete = this.organizationService.search;
  }

  ngOnInit(): void {
    this.formGroup = <FormGroup> this.contactForm;

    this.loadRoles();
  }

  public remove(): void{
    this.removeContact.emit();
  }

  private loadRoles() {
    this.roleService.getRoles$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( roleCodes ) => this.roleCodes = roleCodes );
  }

}
