import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { FormErrorModule } from '../form-error/form-error.module';



@NgModule({
  declarations: [
    ContactComponent,
  ],
  exports: [
    ContactComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgbTypeaheadModule,
    ReactiveFormsModule,
    FormErrorModule,
  ],
})
export class ContactModule { }
