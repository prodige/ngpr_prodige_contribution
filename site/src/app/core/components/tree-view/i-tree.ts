export interface ITree<T>{
  ref: T;
  name: string;
  children?: ITree<T>[];
  expanded: boolean;
  isLoaded?: boolean;
  isLeafNode?: boolean
}
