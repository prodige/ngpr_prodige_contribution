import { Component, Input, OnInit } from '@angular/core';
import { ITree } from './i-tree';

@Component({
  selector:    `alk-tree-view`,
  templateUrl: `./tree-view.component.html`,
  styleUrls:   [`./tree-view.component.scss`],
})
export class TreeViewComponent<T> implements OnInit {
  @Input() tree: ITree<T>[];

  public treeLeaf : ITree<T>[] = [];

  public treeInnerNode: ITree<T>[] = [];

  constructor() { }

  openChildren( node: ITree<T> ){
    console.log( `loadChildren `, node );
    node.expanded = !node.expanded;
  }

  ngOnInit(): void {
    console.log( this.tree );
    this.treeLeaf = this.filterNode( this.tree, `leafNode` );
    this.treeInnerNode = this.filterNode( this.tree, `innerNode` );
  }

  private filterNode( value: ITree<T>[], type: 'leafNode' | `innerNode` ): ITree<T>[] {
    if ( !value ){
      return [];
    }
    switch ( type ){
      case `leafNode`:
        return value.filter(( node ) => !!node.isLeafNode );
      case `innerNode`:
        return value.filter(( node ) => !node.isLeafNode );
      default:
        return value;
    }
  }
}
