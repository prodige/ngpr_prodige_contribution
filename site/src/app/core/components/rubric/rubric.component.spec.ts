import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RubricComponent } from './rubric.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('RubricComponent', () => {
  let component: RubricComponent;
  let fixture: ComponentFixture<RubricComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RubricComponent ],
      imports: [HttpClientTestingModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RubricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
