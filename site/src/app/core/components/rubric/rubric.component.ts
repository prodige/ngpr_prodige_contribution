import { Component, Input, OnInit } from '@angular/core';
import { ITree } from '../tree-view/i-tree';
import { Domain, SubDoamin } from '../../models/domain';
import { takeUntil } from 'rxjs';
import { BaseComponent } from '../base/base.component';
import { RubricService } from './rubric.service';

import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ListItem } from 'ng-multiselect-dropdown/multiselect.model';
import { FormControl } from '@angular/forms';


@Component({
  selector:    `alk-rubric`,
  templateUrl: `./rubric.component.html`,
  styleUrls:   [`./rubric.component.scss`],
})
export class RubricComponent extends BaseComponent implements OnInit {
  @Input()  rubricForm!: FormControl;

  public domainTree: ITree<Domain>[];

  /** Setting des sélection de tags */
  public dropdownSettings: IDropdownSettings = {};


  public formGroup:FormControl = null;

  public subdomains: number[] = [];

  constructor(
    private rubricService: RubricService,
  ) {
    super();

  }

  ngOnInit(): void {
    this.formGroup =  this.rubricForm;
    this.subdomains = <number[]> this.formGroup.value;

    console.log( `RubricComponent Construct`, this.subdomains );
    this.rubricService.getDomainTree$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( domainTree ) =>{
        this.domainTree = domainTree;
        this.rubricService.setSelectedSubDomain( this.subdomains );
      });

    this.dropdownSettings = {
      enableCheckAll:        false,
      singleSelection:       false,
      idField:               `id`,
      textField:             `name`,
      allowSearchFilter:     true,
      searchPlaceholderText: $localize`:@@toResearch:Rechercher`,
      allowRemoteDataSearch: false,
    };


  }

  onSelect( $event: ListItem ){
    const domain = <SubDoamin>$event;

    this.subdomains.push( domain.id );
    this.formGroup.patchValue( this.subdomains );
  }

  onDeSelect( $event: ListItem ){
    const domain = <SubDoamin>$event;

    this.subdomains.splice( this.subdomains.indexOf( domain.id ), 1 );
    this.formGroup.patchValue( this.subdomains );
  }

}
