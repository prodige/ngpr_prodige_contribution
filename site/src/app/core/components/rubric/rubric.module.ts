import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RubricComponent } from './rubric.component';
import { TreeViewModule } from '../tree-view/tree-view.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    RubricComponent,
  ],
  imports: [
    CommonModule,
    TreeViewModule,
    NgMultiSelectDropDownModule,
    FormsModule,
  ],
  exports: [
    RubricComponent,
  ],
})
export class RubricModule { }
