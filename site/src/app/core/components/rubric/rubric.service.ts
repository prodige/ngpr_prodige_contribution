import { Injectable } from '@angular/core';
import { ITree } from '../tree-view/i-tree';
import { Domain, DomainApi, domainApiValidator, DomanApiName, SubDoamin } from '../../models/domain';
import { BehaviorSubject, finalize, map, merge, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { EnvService } from '../../services/env/env.service';

/** interface pour facilité la construction de l'arbre */
interface ITreeBuilder {
  [apiName:string]: ITree<Domain>[];
}


@Injectable({
  providedIn: `root`,
})
export class RubricService {
  private header = { 'Accept': `application/ld+json`, 'Content-Type': `application/ld+json` };

  private options = { withCredentials: true, headers: this.header };

  private domainTree: ITree<Domain>[] = [];
  private domainTree$ = new BehaviorSubject<ITree<Domain>[]>( this.domainTree );

  private loading = false;

  private apiTree: {[key:string]: string} = {
    root:    `rubrics`,
    rubrics: `domains`,
    domains: `subdomains`,
  };

  private apiNameParent: {[key:string]: string} = {
    domains:    `rubric`,
    subdomains: `domain`,
  };

  constructor(
    private httpClient: HttpClient,
    private envService: EnvService,
  ) {

  }

  public getDomainTree$(): Observable<ITree<Domain>[]>{
    if ( this.domainTree.length || this.loading ) {
      return this.domainTree$.asObservable();
    }

    this.loading = true;
    this.buildDomainTree();

    return this.domainTree$.asObservable();
  }

  public setSelectedSubDomain( subdomains: number[]): void{
    if (  !this.domainTree || !subdomains || !( subdomains instanceof Array )){
      return;
    }

    this.domainTree.forEach(( rubric ) =>{
      rubric.ref.domainSelects = [];
      subdomains.forEach(( subdomainId ) => {
        const subDomain = rubric.ref.subDoamins.find(( subdomainTmp ) => subdomainId == subdomainTmp.id );
        if ( subDomain ){
         rubric.ref.domainSelects.push( subDomain );
        }
      });
    });
  }

  private getTreeByApi( apiName: DomanApiName ): Observable<ITree<Domain>[]>{
    return this.httpClient.get<DomainApi>( `${this.envService.urlAdmin}/api/${apiName}?itemsPage=10000000`, this.options )
      .pipe(
        map(( itemApi ) => domainApiValidator.parse( itemApi )),
        map(( rubricApi ) => rubricApi[`hydra:member`]),
        map(( items ) =>
          items.map(( item ) => {
            const ref: {type: string, parentUri?: string, uri: string} = {
              type: apiName,
              uri:  item[`@id`],
            };

            if ( this.apiNameParent[apiName] && item[this.apiNameParent[apiName] as keyof Domain]){
              ref.parentUri = ( <{'@id': string}>item[this.apiNameParent[apiName] as keyof Domain])[`@id`];
            }
            else {
              ref.parentUri = `root`;
            }

            return {
              name:       item.name,
              ref:        Object.assign( ref, item ),
              children:   [],
              expanded:   false,
              isLeafNode: apiName === `subdomains`,
            };
          }),
        ),
      );
  }

  /**
   * Construction récursif de l'arbres
   * la fonction recherche les enfants des nodes, dans l'arbre de référence à plat (treeBuilderRef)
   */
  private buildRecursiveTree( nodes: ITree<Domain>[], treeBuilderRef: ITreeBuilder ): void{
    nodes.forEach(( node, key ) =>{
      const uri = node.ref.uri;
      if ( treeBuilderRef[uri]){
        nodes[key].children = treeBuilderRef[uri];
        this.buildRecursiveTree( treeBuilderRef[uri], treeBuilderRef );
      }
    });
  }

  private buildDomainTree(){
    // Arbre à plat pour faciliter la construction de l'arbre
    const treeTmp:ITreeBuilder = {};

    const obs : Observable<ITree<Domain>[]>[] = [];

    // 1 - on synchronise la récupération des données
    Object.values( this.apiTree ).forEach(( apiName ) => {
      obs.push( this.getTreeByApi( <DomanApiName>apiName  ));
    });

    merge( ...obs ).pipe(
      map(( result ) => {
        if ( !result.length ){
          return;
        }

        // 2 - on commence par construire un arbre à plat où on regroupe les items dans leur parents
        result.forEach(( item ) => {
          if ( !treeTmp[item.ref.parentUri]){
            treeTmp[item.ref.parentUri] = [];
          }

          treeTmp[item.ref.parentUri].push( item );
        });
      }),
      finalize(() => {
        // 3 - on construit l'arbre finale, et envoit les données une fois finis
        console.log( treeTmp );
        this.buildRecursiveTree( treeTmp[`root`], treeTmp );
        console.log( treeTmp );
        this.domainTree = this.subDomainInRubrics( treeTmp[`root`]);
        this.domainTree$.next( this.domainTree );
        this.loading = false;
      }),
    )
      .subscribe();
  }

  private subDomainInRubrics( rubricTree: ITree<Domain>[]): ITree<Domain>[]{
    const retour = rubricTree.map(( rubric ) => {
      let subDomains: SubDoamin[] = [];
      rubric.children.forEach(( domain ) => {
        subDomains = subDomains.concat( domain.children.map(( subDomain )=>(
          subDomain.ref
        )));
      });

      rubric.ref.subDoamins = subDomains;
      rubric.ref.domainSelects = [];

      return rubric;
    });

    console.log( retour );
    return retour;
  }
}
