import { TestBed } from '@angular/core/testing';

import { RubricService } from './rubric.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('RubricService', () => {
  let service: RubricService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
    });
    service = TestBed.inject(RubricService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
