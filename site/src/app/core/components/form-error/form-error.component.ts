import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormGroup } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { ManagedSubdomains } from '../../models/user-right.model';
import { BaseComponent } from '../base/base.component';
import { takeUntil } from 'rxjs';

@Component({
  selector:    `alk-form-error`,
  templateUrl: `./form-error.component.html`,
  styleUrls:   [`./form-error.component.scss`],
})
export class FormErrorComponent extends BaseComponent implements OnInit{
  @Input() control: FormGroup | FormArray | AbstractControl;
  @Input() field!: string | number;

  public ctrlError: AbstractControl;

  public managedSubdomains: ManagedSubdomains[];

  constructor(
    private userService: UserService,
  ) {
    super();
  }


  ngOnInit() {
    if ( !this.control ) { throw new Error( `missing *form* input` ) }
    if ( this.field == undefined ) { throw new Error( `missing *controlName* input` ) }

    if ( this.control instanceof FormArray ) {
      this.ctrlError = this.control.at( <number> this.field );
    } else {

      this.ctrlError = this.control.get( <string> this.field );

      if ( this.field === `subdomains` ){
        this.loadDomains();
      }
      console.log( 22, this.field, this.ctrlError );
    }

  }

  loadDomains(){
    this.userService.getSubDomains$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( result ) =>{
        this.managedSubdomains = result;
      });
  }
}

