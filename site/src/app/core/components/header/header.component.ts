import { Component, OnInit } from '@angular/core';
import { EnvService } from '../../services/env/env.service';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CasService } from '../../services/cas/cas.service';
import { BaseComponent } from '../base/base.component';
import { UserService } from '../../services/user.service';
import { takeUntil } from 'rxjs';
import { User } from '../../models/user';
import { ModalConfirmComponent } from '../../modals/modal-confirm/modal-confirm.component';

@Component({
  selector:    `alk-header`,
  templateUrl: `./header.component.html`,
  styleUrls:   [`./header.component.scss`],
})
export class HeaderComponent extends BaseComponent implements OnInit{
  public user: User;

  private geonetowrkHomeParams = `/geonetwork/srv/fre/catalog.edit#/` +
    `board?sortBy=dateStamp&sortOrder=desc&isTemplate=%5B%22y%22,%22n%22%5D&` +
    `resultType=manager&from=1&to=20&query_string=%7B%22resourceType%22:%7B%22dataset%22:true%7D%7D&owner=40102`;

  private message = $localize`:@@headerConfirm:Êtes-vous sûr de vouloir quitter ce menu ?  Toutes les informations non enregistrées seront perdues.`;

  constructor(
    private envService: EnvService,
    private ngbModal: NgbModal,
    private casService: CasService,
    private userService: UserService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.userService.getUser$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( user ) => {
        this.user = user;
      });
  }

  gotToGeonetwork(){
    const modalRef = this.ngbModal.open( ModalConfirmComponent );
    ( <ModalConfirmComponent>modalRef.componentInstance ).message = this.message;


    modalRef.result.then(( confirmation ) => {
      if ( confirmation ) {
        window.location.href = `${this.envService.catalogueUrl}${this.geonetowrkHomeParams}`;
      }
    })
      .catch(( err ) => {
        console.error( err );
      });
  }

  loggout(){
    const modalRef = this.ngbModal.open( ModalConfirmComponent );
    ( <ModalConfirmComponent>modalRef.componentInstance ).message = this.message;

    modalRef.result.then(( confirmation ) => {
      if ( confirmation ) {
        this.casService.logout();
      }
    })
      .catch(( err ) => {
        console.error( err );
      });
  }
}
