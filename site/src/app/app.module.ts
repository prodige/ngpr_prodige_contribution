import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderModule } from "./core/components/header/header.module";
import { FooterModule } from "./core/components/footer/footer.module";
import { EnvServiceProvider } from "./core/services/env/env.service.provider";
import { HttpClientModule } from "@angular/common/http";
import { CommonModule } from "@angular/common";
import { MapModule, VisualiseurCoreModule } from '@alkante/visualiseur-core';
import { DragulaModule } from 'ng2-dragula';
import { LoggerModule } from './core/components/logger/logger.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    NgbModule,
    HeaderModule,
    FooterModule,
    HttpClientModule,
    VisualiseurCoreModule,
    MapModule,
    DragulaModule,
    LoggerModule,
  ],
  providers: [EnvServiceProvider],
  bootstrap: [AppComponent],
})
export class AppModule { }
