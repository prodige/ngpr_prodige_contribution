# Changelog

All notable changes to [ngpr_prodige_contribution](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution) project will be documented in this file.

## [5.0.33](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.32...5.0.33) - 2025-03-03

## [5.0.32](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.31...5.0.32) - 2025-02-12

## [5.0.31](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.30...5.0.31) - 2025-01-08

## [5.0.30](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.29...5.0.30) - 2024-10-22

## [5.0.29](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.28...5.0.29) - 2024-10-09

## [5.0.28](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.27...5.0.28) - 2024-09-03

## [5.0.27](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.26...5.0.27) - 2024-08-27

## [5.0.26](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.25...5.0.26) - 2024-02-27

## [5.0.25](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.24...5.0.25) - 2024-02-23

## [5.0.24](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.23...5.0.24) - 2024-02-21

## [5.0.23](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.22...5.0.23) - 2024-01-24

## [5.0.22](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.21...5.0.22) - 2024-01-23

## [5.0.21](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.20...5.0.21) - 2024-01-22

## [5.0.20](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.19...5.0.20) - 2024-01-19

## [5.0.19](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.18...5.0.19) - 2024-01-02

## [5.0.18](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.17...5.0.18) - 2023-09-29

## [5.0.17](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.16...5.0.17) - 2023-06-02

## [5.0.16](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.15...5.0.16) - 2023-06-02

## [5.0.15](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.14...5.0.15) - 2023-06-02

## [5.0.14](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.13...5.0.14) - 2023-06-02

## [5.0.13](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.12...5.0.13) - 2023-06-01

## [5.0.12](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.11...5.0.12) - 2023-05-30

## [5.0.11](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.10...5.0.11) - 2023-04-27

## [5.0.10](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.9...5.0.10) - 2023-04-11

## [5.0.9](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.8...5.0.9) - 2023-03-23

## [5.0.8](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.7...5.0.8) - 2023-03-21

## [5.0.7](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.6...5.0.7) - 2023-03-20

## [5.0.6](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.5...5.0.6) - 2023-03-15

## [5.0.5](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.4...5.0.5) - 2023-03-08

## [5.0.4](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.3...5.0.4) - 2023-03-08

## [5.0.3](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.2...5.0.3) - 2023-03-07

## [5.0.2](https://gitlab.adullact.net/prodige/ngpr_prodige_contribution/compare/5.0.1...5.0.2) - 2023-03-07

