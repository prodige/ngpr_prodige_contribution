#!/bin/bash
set -e

SCRIPTDIR="$( cd $(dirname ${BASH_SOURCE[0]}/) && pwd )"
PROJECTDIR="$( cd ${SCRIPTDIR}/../..  && pwd )"
PROJECTNAME="$(basename $PROJECTDIR )"
NODE_CONTAINER_NAME="${PROJECTNAME}_dev_node"

if [ "$1" == "debug" ]; then
  echo -e "SCRIPTDIR=${SCRIPTDIR}";
  echo -e "PROJECTDIR=${PROJECTDIR}";
  echo -e "PROJECTNAME=${PROJECTNAME}";
  echo -e "NODE_CONTAINER_NAME=${NODE_CONTAINER_NAME}";
  exit
fi

# On se place dans le répertoire du script (cicd/dev)
cd ${SCRIPTDIR}

# ID user for mapping in docker
echo -e "LOCAL_USER_ID=$(id -u)\nLOCAL_GROUP_ID=$(id -g)" > docker-compose.env

if [ "$1" == "down" ]; then
  # Stop and remove containers, networks, images, and volumes
  echo -e "docker-compose down -t 0";
  docker-compose down -t 1;
  exit;
else

  if [ "$( docker container inspect -f '{{.State.Status}}' ${NODE_CONTAINER_NAME} )" == "running" ]; then
    # On down le précédent container si déjà lancé
    docker-compose down;
  fi

  # Run container
  echo -e "docker-compose up --build";
  docker-compose up --build -d
fi
