#!/bin/bash
set -e

SCRIPTDIR="$( cd $(dirname ${BASH_SOURCE[0]}/) && pwd )"
PROJECTDIR="$( cd ${SCRIPTDIR}/../../..  && pwd )"
PROJECTNAME="$(basename $PROJECTDIR )"
NODE_CONTAINER_NAME="${PROJECTNAME}_dev_node"

# On vérifie que le docker du projet tourne
if [ ! "$( docker container inspect -f '{{.State.Status}}' $NODE_CONTAINER_NAME )" == "running" ]; then
    echo -e "docker container $NODE_CONTAINER_NAME is not running"
    exit
fi

echo -e "cd ${PROJECTDIR}"
cd ${PROJECTDIR}

# GESTION DU PROJET
if [ ! -d "${PROJECTDIR}/site/node_modules" ]; then
  # On lance le npm install si il n'a pas encore été éxécuté
  echo -e "docker exec -t --user node -w /home/node/app ${NODE_CONTAINER_NAME} npm install"
  docker exec -t --user node -w /home/node/app ${NODE_CONTAINER_NAME} npm install;
else
  echo -e "Dependencies already installed for ${PROJECTNAME}."
fi

echo "-- Prepare done --";
