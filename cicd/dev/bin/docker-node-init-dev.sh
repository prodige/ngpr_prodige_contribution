#!/bin/bash
set -e

SCRIPTDIR="$( cd $(dirname ${BASH_SOURCE[0]}/) && pwd )"
PROJECTDIR="$( cd ${SCRIPTDIR}/../../..  && pwd )"
PROJECTNAME="$(basename $PROJECTDIR )"
NODE_CONTAINER_NAME="${PROJECTNAME}_dev_node"

# Si le docker tourne, il faut d'abord l'éteindre pour pouvoir supprimer le volume
if [ ! "$( docker container inspect -f '{{.State.Status}}' $NODE_CONTAINER_NAME )" == "running" ]; then
    echo -e "docker container $NODE_CONTAINER_NAME is not running"
    exit
fi

if [ ! -f "${PROJECTDIR}/site/angular.json" ]; then
  # Si le projet angular n'a pas déjà été initialisé
  # on le génère
  docker exec -it --user node -w /home/node/app ${NODE_CONTAINER_NAME} bash -c "ng new ngpr_prodige_contribution --strict --routing --prefix alk --package-manager npm --style scss --skip-git";
  # puis on remonte ce qui a été généré d'un niveau pour être juste dans site
  shopt -s dotglob nullglob
  mv $PROJECTDIR/site_/${PROJECTNAME}/* $PROJECTDIR/site_/
  rm -rf $PROJECTDIR/site_/${PROJECTNAME}
else
  echo -e "Il semble que le projet ait déjà été initialisé"
fi
