#!/bin/bash
set -e

SCRIPTDIR="$( cd $(dirname ${BASH_SOURCE[0]}/) && pwd )"
PROJECTDIR="$( cd ${SCRIPTDIR}/../../..  && pwd )"
PROJECTNAME="$(basename $PROJECTDIR )"
NODE_CONTAINER_NAME="${PROJECTNAME}_dev_node"

# Si le docker tourne, il faut d'abord l'éteindre pour pouvoir supprimer le volume
if [ ! "$( docker container inspect -f '{{.State.Status}}' $NODE_CONTAINER_NAME )" == "running" ]; then
    echo -e "docker container $NODE_CONTAINER_NAME is not running"
    exit
fi

if [ -f "${PROJECTDIR}/site/angular.json" ]; then
  echo -e "docker exec -it --user node -w /home/node/app ${NODE_CONTAINER_NAME} ng serve --public-host localhost:4200 --host 0.0.0.0 --port 4200 --disable-host-check true;";
  docker exec -it --user node -w /home/node/app ${NODE_CONTAINER_NAME} ng serve --public-host localhost:4200 --host 0.0.0.0 --port 4200 --disable-host-check true;
else
  echo -e "Le projet angular n'a pas encore été initialisé. Veuillez jouer docker-node-init-dev.sh";
fi
