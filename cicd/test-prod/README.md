# test-prod

Run dockers built previously by ```../build/README.md```


```bash
# Login on Alkante docker registry
#docker login docker.alkante.com

# Create dockers
docker-compose up
```

url : http://localhost:4200

Stop dockers with
```bash
docker-compose down
```
