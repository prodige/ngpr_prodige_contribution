# ngpr_prodige_contribution

> :warning: **Ne pas jouer les commandes AngularCli (`ng ...`) ou `npm install` depuis sa machine.**
Se connecter au container docker pour le faire. Celui-ci possède les bonnes versions de node, npm, angular-cli, typescript ( les mêmes que les autres personnes travaillant sur le projet).

## Pour démarrer de Container docker de dev
container démarrer avec le pr_prodige v5

## Pour se connecter au Container docker de dev
Pour ajouter des dépendances via `npm install [nom de la dépendance]` ou pour générer des éléments avec des commandes `ng ...` par exemple.
```bash
docker exec --user node -w /home/node/app/site -it ngpr_prodige_contribution_web bash
```

## Pour l'installation (depuis le container)
```bash
npm run login
npm install
```

## Pour lancer le serveur
```bash
docker exec --user node -w /home/node/app/site -it ngpr_prodige_contribution_web npm run dev
```

## Pour lancer les tests unitaires
```bash
./cicd/dev/docker-compose.sh 
docker exec --user node -w /home/node/app -it ngpr_prodige_contribution_dev_node npm run test
```
